<?php

namespace App\Http\Controllers\Employee;

use App\Http\Requests\Employee\EmployeeRequestValidation;
use App\Model\Announcements\CreateAnnouncement;
use App\Model\Employee;
use App\Model\Master\JobTitle;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class SelfService extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api-employee');
    }




    public function ChangePassword(Request $request){

        $user = $request->user();

        $f = Employee::where('id',$user->id )->first();

        $f->password = bcrypt($request->password);

        $f->save();


        return response()->json('success',200);

        }


        public function GetProfile(Request $request){

        $user = $request->user();

        $f = Employee::where('id',$user->id )->with([

            'create_employee_to_additional_profiles',
            'create_employee_to_pictures',

        ])->first();

        return response()->json($f,200);

        }


        public function EmployeeRequest(EmployeeRequestValidation  $request){

        $user = $request->user();

        $f = new Employee\CreateEmployeeToRequests();



        if ($request->image != null){

            $imageName1 = time().'.'.$request->image->getClientOriginalExtension();
            $imageName2 = str_replace(' ', '_', $imageName1);
            $imageName = str_replace(' ', '_', $imageName1);
            $image = $request->file('image');
            $t = Storage::disk('public')->put('images/employee/request/'.$imageName, file_get_contents($image));
            //$imageName = Storage::disk('s3')->url('images/employee/profile/'.$imageName);
            // $imageName3 = Storage::disk('s3')->url('product_image/primary/'.$imageName);

            //  $replacesextraspaces1 = str_replace(' ', '_', $finalimageurl1);

            $location = '/storage/images/employee/request/'.$imageName;

            $f->create_employee_id = $user->id;
            $f->title = $request->title;
            $f->category = $request->category;
            $f->description = $request->description;
            $f->image_title = $request->image_title;
            $f->image_location = $location;
            $f->to_request = $request->to_request;
            $f->save();
        } else {


            $f->create_employee_id = $user->id;
            $f->title = $request->title;
            $f->category = $request->category;
            $f->description = $request->description;
            $f->image_title = $request->image_title;
            $f->to_request = $request->to_request;
            $f->save();



        }

        return response()->json($f,200);

        }



        public function ImageUpload(Request $request){


           // if ($request->image != null){


                $file_data = $request->input('image_file');
                $file_name = 'image_' . time() . '.png'; //generating unique file name;

                if ($file_data != "") { // storing image in storage/app/public Folder
                    Storage::disk('public')->put('images/daily_report/image/'.$file_name, base64_decode($file_data));

                    $filelocation = 'images/daily_report/image/'.$file_name;
                    return response()->json($filelocation);

                }


          //  $t = Storage::disk('public')->put('images/employee/profile/'.$imageName, file_get_contents($image));

            return response()->json('no image');

             //   $location = '/storage/images/employee/request/'.$imageName;



          //  }


        }
        public function DailyReport(Request $request){

            $user = $request->user();

            $f = new Employee\CreateEmployeeToDailyReport();


            $mytime = Carbon::now();





                $f->create_employee_id = $user->id;
                $f->day = $mytime->toDay();
                $f->month = $mytime->format('F');
                $f->year = $mytime->format('Y');
                $f->title = $request->title;
                $f->description = $request->description;
                $f->image_location = $request->image_location;
                $f->lang = $request->lang;
                $f->long = $request->long;
                $f->ip = $request->ip();
                $f->save();


            return response()->json($f,200);

        }

        public function DailyReportbetweentwodatesget($from, $to, Request $request){

          $user = $request->user();

         //   $f = new Employee\CreateEmployeeToDailyReport();


            $b = Employee\CreateEmployeeToDailyReport::where('create_employee_id' , $user->id)->get();


         $a =    $b->whereBetween('created_at', [$from." 00:00:00", $to." 00:00:00"])->get();


            return response()->json($a,200);

        }


        public function Employeeexpenses(Request $request){

            $user = $request->user();

            $f = new Employee\CreateEmployeeToExpences();


            $mytime = Carbon::now();





                $f->create_employee_id = $user->id;
                $f->image_title = $request->image_title;
                $f->image_location = $request->image_location;
                $f->price = $request->price;
                $f->title = $request->title;
                $f->description = $request->description;
                $f->lang = $request->lang;
                $f->long = $request->long;
                $f->ip = $request->ip();
                $f->save();


            return response()->json($f,200);

        }



        public function EmployeeLeads(Request $request){

            $user = $request->user();

            $f = new Employee\CreateEmployeeToLeads();


            $mytime = Carbon::now();





                $f->create_employee_id = $user->id;
                $f->name = $request->name;
                $f->referer = $request->referer;
                $f->mobile = $request->mobile;
                $f->email = $request->email;
                $f->address = $request->address;
                $f->description = $request->description;
                $f->lat = $request->lat;
                $f->image = $request->image;
                $f->long = $request->long;
                $f->save();


            return response()->json($f,200);

        }


        public function GetEmployeeLeads(Request $request){

            $user = $request->user();

            $f =  Employee\CreateEmployeeToExpences::where('create_employee_id',$user->id)->paginate(12);




            return response()->json($f,200);

        }


        public function LiveLocation(Request $request){

            $user = $request->user();


            $a = Employee\CreateEmployeeToLiveLocation::where('create_employee_id',$user->id)
                ->where('created_at', '>=', Carbon::now()->subDay()->toDateTimeString())
                ->where('lat', $request->lat )
                ->where('long', $request->long )
                ->get();



            if (count($a) < 1){
                $f = new Employee\CreateEmployeeToLiveLocation();






                $f->create_employee_id = $user->id;
                $f->lat = $request->lat;
                $f->long = $request->long;
                $f->is_moving = $request->is_moving;
                $f->save();
                return response()->json($a,200);
            } else {
                return response()->json([
                    'data' => $a,
                    'exists' => true
                ], 200);
            }







        }

        public function LiveLocationLogin(Request $request){

            $user = $request->user();

            $f = new Employee\EmployeeToTrackLoginLogout();






                $f->create_employee_id = $user->id;
                $f->lat = $request->lat;
                $f->long = $request->long;
                $f->is_moving = $request->is_moving;
                $f->type = $request->type;
                $f->save();





            return response()->json($f,200);

        }


        public function Announcement(Request $request){

            $user = $request->user();

            $f = CreateAnnouncement::latest()->get();




            return response()->json($f,200);

        }

        public function GetCategory(Request $request){

            $user = $request->user();

            $getCategory = $user->category;

            $searchTTL = JobTitle::with('tracking_masters')->where('name', $getCategory)->first();

            return response()->json($searchTTL,200);






        }





}
