<?php

namespace App\Http\Controllers\Employee\web;

use App\Model\Employee\CreateEmployee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class EmployeeAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:web-employee', ['except' => ['logout']]);
    }



    public function loginPage(){

        return view('employeeauth.login');

    }





    public function CustomerLogin(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:employees,email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->only('email', 'remember'));
        }


        // Attempt to log the user in
        if (Auth::guard('web-employee')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('employee.dashboard'));
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect() ->back()
            ->withErrors($validator, 'password')
            ->withInput($request->only('email', 'remember'));

    }

    public function logout()
    {
        Auth::guard('web-employee')->logout();
        return redirect('/');
    }
}
