<?php

namespace App\Http\Controllers\Employee\web;

use App\Model\Employee;
use App\Model\Employee\DailyReportToFile;
use App\Model\Employee\DailyReportToImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Carbon\Carbon;
use App\Model\Employee\CreateEmployeeToDailyReport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:web-employee');
    }

    public function ChangePassword(Request $request){
        $user = Auth::user();

        $a = Employee::findorfail($user->id);
        $a->password = $request->password;

        $a->save();

        return back();

    }



    public function dashboard(){

        return view('employeedashboard.page.dailyreport.all');

    }

    public function CreatedaylyReportCreate(){

        return view('employeedashboard.page.dailyreport.create');

    }

    public function CreatedaylyReportsave(Request $request){

        $f = new CreateEmployeeToDailyReport();


        $mytime = Carbon::now();





        $f->create_employee_id = Auth::id();
        $f->day = $mytime->toDay();
        $f->month = $mytime->format('F');
        $f->year = $mytime->format('Y');
        $f->title = $request->title;
        $f->description = $request->description;
        $f->image_location = null;
        $f->lang = $request->lang;
        $f->long = $request->long;
        $f->ip = $request->ip();
        $f->save();

        return redirect(route('employee.daily.report.details',$f->id));

    }

    public  function ViewDailyReport($id){






        $f = CreateEmployeeToDailyReport::findorfail($id);


      //  $f = CreateEmployeeToDailyReport::findorfail($id);

        $img = DailyReportToImages::where('create_employee_to_daily_report_image_id', $id)->get();
        $file = DailyReportToFile::where('create_employee_to_daily_report_image_id', $id)->get();

       // dd($img);

        $client = new \GuzzleHttp\Client();

// Create a request
        $request = $client->get('http://ip-api.com/json/'.$f->ip);
// Get the actual response without headers
        $response = $request->getBody();

        $res = json_decode($response);

        //  return $response;









        return view('employeedashboard.page.dailyreport.details')->with(['r' => $f, 'res' => $res,'imgs'=>$img,'files'=> $file]);









    }

    public function alllDailyResort(){


    }

    public function DailyReportDatables(){


        $user = Auth::id();

        $invoices = CreateEmployeeToDailyReport::where('create_employee_id',$user)->select(
            [
                'id',
                'create_employee_id',
                'day',
                'month',
                'year',
                'title',
                'lang',
                'long',
                'ip',
                'created_at',
                'image_location'
            ]);



        return DataTables::of($invoices)
            ->editColumn('created_at', function($invoice) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->toDayDateTimeString();
            })



            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('employee.daily.report.details',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="fa fa-arrow-right"></i>View Details</a>';
            })


            ->rawColumns(['action','delete'])
            ->orderColumn('id', 'created_at $1')
            ->make();



    }


    public function UploadImage($id, Request $request){





        $imageName1 = time().'.'.$request->image->getClientOriginalExtension();
        $imageName2 = str_replace(' ', '_', $imageName1);
        $imageName = str_replace(' ', '_', $imageName1);
        $image = $request->file('image');
        $t = Storage::disk('public')->put('images/employee/daily-report/images/'.$imageName, file_get_contents($image));
        //$imageName = Storage::disk('s3')->url('images/employee/profile/'.$imageName);
        // $imageName3 = Storage::disk('s3')->url('product_image/primary/'.$imageName);

        //  $replacesextraspaces1 = str_replace(' ', '_', $finalimageurl1);

        $location = $request->getHttpHost().'/storage/images/employee/daily-report/images/'.$imageName;





        $s = new DailyReportToImages();
        $s->create_employee_to_daily_report_image_id = $id;
        $s->image_location = $location;
        $s->image_title = $imageName;
        $s->save();

        return response()->json('success',200);








    }





    public function UploadFiles($id, Request $request){





        $imageName1 = time().'.'.$request->image->getClientOriginalExtension();
        $imageName2 = str_replace(' ', '_', $imageName1);
        $imageName = str_replace(' ', '_', $imageName1);
        $image = $request->file('image');
        $t = Storage::disk('public')->put('images/employee/daily-report/files/'.$imageName, file_get_contents($image));
        //$imageName = Storage::disk('s3')->url('images/employee/profile/'.$imageName);
        // $imageName3 = Storage::disk('s3')->url('product_image/primary/'.$imageName);

        //  $replacesextraspaces1 = str_replace(' ', '_', $finalimageurl1);

        $location = $request->getHttpHost().'/storage/images/employee/daily-report/files/'.$imageName;





        $s = new DailyReportToFile();
        $s->create_employee_to_daily_report_image_id = $id;
        $s->file_location = $location;
        $s->file_title = $imageName;
        $s->save();

        return response()->json('success',200);








    }







}
