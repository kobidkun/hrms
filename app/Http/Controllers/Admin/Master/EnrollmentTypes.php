<?php

namespace App\Http\Controllers\Admin\Master;

use App\Model\Master\EnrollmentType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EnrollmentTypes extends Controller
{
    public function allenroll(){

        $a = EnrollmentType::all();

        return view('dashboard.page.admin.enroltype.all')->with(['a' => $a]);


    }


    public function Create(){

        $a = EnrollmentType::all();

        return view('dashboard.page.admin.enroltype.create')->with(['e' => $a]);


    }

    public function Save(Request $request){

        $a = new EnrollmentType();
        $a->name = $request->name;
        $a->description = $request->description;
        $a->slug = $request->slug;

        $a->save();

        return redirect( )->back();

    }

    public function Delete($id){


        $a = EnrollmentType::findorfail($id);

        $a->delete();

        return back();



    }


}
