<?php

namespace App\Http\Controllers\Admin\Master;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MiscController extends Controller
{
   public function ChangeAdminPassword(){
       return  view('dashboard.page.admin.misc.password');
   }

   public function ChangeAdminPasswordStore(Request $request){
       $user = Auth::user();

       $a = User::findorfail($user->id);
       $a->password = bcrypt($request->password);

       $a->save();

       return back();


   }

}
