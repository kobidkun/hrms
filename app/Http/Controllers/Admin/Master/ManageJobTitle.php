<?php

namespace App\Http\Controllers\Admin\Master;

use App\Model\Master\JobTitle;
use App\Model\Master\TrackingMaster;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageJobTitle extends Controller
{
    public function Create(){


        return view('dashboard.page.admin.jobtitle.create');

    }


    public function EditCategory($id){

        $a = JobTitle::findorfail($id);

        return view('dashboard.page.admin.jobtitle.edit')->with(['d'=>$a]);

    }


    public function UpdateCategory($id, Request $request){

        $a = JobTitle::findorfail($id);

        $a->name = $request->name;
        $a->description = $request->description;
        $a->slug = $request->slug;
        $a->save();

        if (TrackingMaster::where('category_id', $id)->exists()){

            $b = TrackingMaster::where('category_id', $id)->first();

            $b->location_provider = $request->location_provider;
            $b->accuracy = $request->accuracy;
            $b->stationary_radius = $request->stationary_radius;
            $b->debug = $request->debug;
            $b->distance_filter = $request->distance_filter;
            $b->interval = $request->interval;
            $b->fastest_interval = $request->fastest_interval;
            $b->activity_interval = $request->activity_interval;
            $b->stop_still = $request->stop_still;
            $b->start_foreground = $request->start_foreground;


            $b->save();

        } else {


            $b = new TrackingMaster();
            $b->category_id = $a->id;
            $b->location_provider = $request->id;
            $b->accuracy = $request->accuracy;
            $b->stationary_radius = $request->stationary_radius;
            $b->debug = $request->debug;
            $b->distance_filter = $request->distance_filter;
            $b->interval = $request->interval;
            $b->fastest_interval = $request->fastest_interval;
            $b->activity_interval = $request->activity_interval;
            $b->stop_still = $request->stop_still;
            $b->start_foreground = $request->start_foreground;


            $b->save();

        }







        return back();



    }

    public function AllTitles(){


        $a = JobTitle::all();

        return view('dashboard.page.admin.jobtitle.all')->with(['a' => $a]);



    }


    public function Save(Request $request){

        $a = new JobTitle();
        $a->name = $request->name;
        $a->description = $request->description;
        $a->slug = $request->slug;
        $a->save();

        $b = new TrackingMaster();
        $b->category_id = $a->id;
        $b->location_provider = $request->id;
        $b->accuracy = $request->accuracy;
        $b->stationary_radius = $request->stationary_radius;
        $b->debug = $request->debug;
        $b->distance_filter = $request->distance_filter;
        $b->interval = $request->interval;
        $b->fastest_interval = $request->fastest_interval;
        $b->activity_interval = $request->activity_interval;
        $b->stop_still = $request->stop_still;
        $b->start_foreground = $request->start_foreground;


        $b->save();




        return back();

    }

    public function delete($id){

        $a = JobTitle::findorfail($id);

        $a->delete();

        return back();



    }




}
