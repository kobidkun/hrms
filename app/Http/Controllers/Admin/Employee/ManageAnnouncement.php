<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Model\Announcements\CreateAnnouncement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageAnnouncement extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:web');
    }


   public function Create(){


       $a = CreateAnnouncement::all();

       return view('dashboard.page.admin.announcement.create')->with(['a' => $a]);



   }

   public function Save(Request $request){

       $a = new CreateAnnouncement();
       $a->title = $request->name;
       $a->description = $request->description;
       $a->slug = $request->slug;
       $a->save();
       return back();

   }

   public function Delete($id){

       $a =  CreateAnnouncement::findorfail($id);

       $a->delete();

       return back();

   }


    function random_str($type = 'alphanum', $length = 20)
    {
        switch($type)
        {
            case 'basic'    : return mt_rand();
                break;
            case 'alpha'    :
            case 'alphanum' :
            case 'num'      :
            case 'nozero'   :
                $seedings             = array();
                $seedings['alpha']    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alphanum'] = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['num']      = '0123456789';
                $seedings['nozero']   = '123456789';

                $pool = $seedings[$type];

                $str = '';
                for ($i=0; $i < $length; $i++)
                {
                    $str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
                }
                return $str;
                break;
            case 'unique'   :
            case 'md5'      :
                return md5(uniqid(mt_rand()));
                break;
        }
    }

}
