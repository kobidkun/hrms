<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Model\Employee\CreateEmployeeToExpences;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
class ManageExpences extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function RequestDatatable($id){

//   $Requestload = CreateEmployee\CreateEmployeeToRequests::where('create_employee_id',$id)->get();

        $invoices = CreateEmployeeToExpences::where('create_employee_id',$id)->select(
            [
                'id',
                'create_employee_id',
                'title',
                'description',
                'price',
                'created_at',
            ]);



        return DataTables::of($invoices)
            ->editColumn('created_at', function($invoice) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->toDayDateTimeString();
            })



            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('super.employee.expences.get.details',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="fa fa-arrow-right"></i>View Details</a>';
            })

            ->addColumn('price', function ($invoice) {
                return '<button class=" btn btn-xs btn-success" title="'.$invoice->price.'"><i class="fa fa-rupee"></i>'.$invoice->price.'</button>';
            })


            ->rawColumns(['action','price'])
            ->orderColumn('id', 'created_at $1')
            ->make();

    }



    public function ViewDailyReportEmployee($id){












        $f = CreateEmployeeToExpences::findorfail($id);




        $client = new \GuzzleHttp\Client();

// Create a request
        $request = $client->get('http://ip-api.com/json/'.$f->ip);
// Get the actual response without headers
        $response = $request->getBody();

        $res = json_decode($response);

        //  return $response;









        return view('dashboard.page.admin.employee.sub.expence')->with(['r' => $f, 'res' => $res]);


    }
}
