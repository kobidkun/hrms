<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Model\Employee\CreateEmployeeToDailyReport;
use App\Model\Employee\DailyReportToFile;
use App\Model\Employee\DailyReportToImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
class ManageDailyReport extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }


    public function RequestDatatable($id){

//   $Requestload = CreateEmployee\CreateEmployeeToRequests::where('create_employee_id',$id)->get();

        $invoices = CreateEmployeeToDailyReport::where('create_employee_id',$id)->select(
            [
                'id',
                'create_employee_id',
                'day',
                'month',
                'year',
                'title',
                'lang',
                'long',
                'ip',
                'created_at',
                'image_location'
            ]);



        return DataTables::of($invoices)
            ->editColumn('created_at', function($invoice) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->toDayDateTimeString();
            })



            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('super.employee.daily.get.report',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="fa fa-arrow-right"></i>View Details</a>';
            })


            ->rawColumns(['action','delete'])
            ->orderColumn('id', 'created_at $1')
            ->make();

    }


    public function ViewDailyReportEmployee($id){












        $f = CreateEmployeeToDailyReport::findorfail($id);



        $img = DailyReportToImages::where('create_employee_to_daily_report_image_id', $id)->get();
        $file = DailyReportToFile::where('create_employee_to_daily_report_image_id', $id)->get();


        $client = new \GuzzleHttp\Client();

// Create a request
        $request = $client->get('http://ip-api.com/json/'.$f->ip);
// Get the actual response without headers
        $response = $request->getBody();

        $res = json_decode($response);

      //  return $response;









        return view('dashboard.page.admin.employee.dailyreportdetails')->with(['r' => $f, 'res' => $res,'imgs' => $img,'files'=> $file]);


    }


    public function DeleteFile($id){

        $a = DailyReportToImages::findorfail($id);

        $a->file_location = null;
        $a->file_title = 'File Deleted';
        $a->save();

        return back();


    }

    public function DeleteImages($id){

        $a = DailyReportToFile::findorfail($id);

        $a->image_location = null;
        $a->image_title = 'File Deleted';
        $a->save();

        return back();


    }
}
