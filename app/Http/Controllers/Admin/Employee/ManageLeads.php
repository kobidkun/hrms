<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Model\Employee\CreateEmployeeToLeads;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
class ManageLeads extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function RequestDatatable($id){

//   $Requestload = CreateEmployee\CreateEmployeeToRequests::where('create_employee_id',$id)->get();

        $invoices = CreateEmployeeToLeads::where('create_employee_id',$id)->select(
            [
                'id',
                'create_employee_id',
                'name',
                'referer',
                'mobile',
                'email',
                'address',
                'description',
                'long',
                'created_at',
                'image'
            ]);



        return DataTables::of($invoices)
            ->editColumn('created_at', function($invoice) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->toDayDateTimeString();
            })



            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('super.employee.request.get.leads.details',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="fa fa-arrow-right"></i>View Details</a>';
            })


            ->rawColumns(['action','delete'])
            ->orderColumn('id', 'created_at $1')
            ->make();

    }


    public function ViewDailyReportEmployee($id){












        $f = CreateEmployeeToLeads::findorfail($id);




        $client = new \GuzzleHttp\Client();

// Create a request
        $request = $client->get('http://ip-api.com/json/'.$f->ip);
// Get the actual response without headers
        $response = $request->getBody();

        $res = json_decode($response);

        //  return $response;









        return view('dashboard.page.admin.employee.sub.leadsdetails')->with(['r' => $f, 'res' => $res]);


    }
}
