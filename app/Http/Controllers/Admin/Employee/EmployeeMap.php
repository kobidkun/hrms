<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Model\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class EmployeeMap extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:web');
    }

   public function MapView ($id, Request $request ){

       $a = Employee::findorfail($id);

       $b = Employee\CreateEmployeeToLiveLocation::where('create_employee_id', $id)->orderBy('id', 'DESC')->first();


       return view('dashboard.page.admin.employee.mapview')->with(['loc' => $b,'e' =>$a]);

   }

   public function MapViewDayview ($id, Request $request ){

       $a = Employee::findorfail($id);

       $b = Employee\CreateEmployeeToLiveLocation::where('create_employee_id', $id)->get();

       $from = date($request->date);
       $to = date($request->date);

    //   $data =  $b->whereDate('created_at', [$from, $to])->get();

    //   $yesterday = date("Y-m-d", strtotime( '-1 days' ) );
       $countYesterday = Employee\CreateEmployeeToLiveLocation::where('create_employee_id', $a->id)
           ->whereDate('created_at', $from )
           ->get();


     //  return response()->json($countYesterday);

     //  dd($countYesterday);
  return view('dashboard.page.admin.employee.dailymapview')->with([
      'ds' => $countYesterday ,
      'date' => $from,
      'id' => $request->id

  ]);

    //   return response()->json($countYesterday);

       //  return view('dashboard.page.admin.employee.mapview')->with(['loc' => $b,'e' =>$a]);

   }





     public function RequestDatatablelivelocation($id,$date, Request $request){

//   $Requestload = CreateEmployee\CreateEmployeeToRequests::where('create_employee_id',$id)->get();



        // $from = date($date);



         $employee = Employee::findorfail($id);

      // $location2 =  $employee->create_employee_to_live_locations()->distinct('lat')->toArray();






         $invoices = Employee\CreateEmployeeToLiveLocation::where('create_employee_id', $id)
             ->whereDate('created_at', $date )
             ->select(
                 [
                     'id',
                     'create_employee_id',
                     'lat',
                     'long',
                     'created_at'
                 ]);







        return DataTables::of($invoices)
            ->editColumn('created_at', function($invoice) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->toDayDateTimeString();
            })



            ->addColumn('action', function ($invoice) {
                return
                    '<a target="_blank" href="http://maps.google.com/maps?q='.$invoice->lat.','.$invoice->long.'" class=" btn btn-xs btn-primary" title="View details"><i class="fa fa-arrow-right"></i>View Details</a>';
            })


            ->rawColumns(['action'])
            ->orderColumn('id', 'created_at $1')
            ->make();

    }


     public function RequestDatatableloginlogout($date,$id, Request $request){

//   $Requestload = CreateEmployee\CreateEmployeeToRequests::where('create_employee_id',$id)->get();

        $a = Employee::findorfail($id);

         $from = date($date);

        $invoices = Employee\EmployeeToTrackLoginLogout::where('create_employee_id',$id)->whereDate('created_at', $from )
            ->select([
                'id',
                'create_employee_id',
                'lat',
                'long',
                'type',
                'created_at'
            ]);



        return DataTables::of($invoices)
            ->editColumn('created_at', function($invoice) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->toDayDateTimeString();
            })



            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('super.employee.details',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="fa fa-arrow-right"></i>View Details</a>';
            })


            ->rawColumns(['action'])
            ->orderColumn('id', 'created_at $1')
            ->make();

    }









}
