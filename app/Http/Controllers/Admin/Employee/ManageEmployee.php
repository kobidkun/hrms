<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Requests\Employee\CreateEmployeeRequest;
use App\Model\Employee as CreateEmployee;
use App\Model\Employee\CreateEmployeeToPicture;
use App\Model\Master\EnrollmentType;
use App\Model\Master\JobTitle;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class ManageEmployee extends Controller
{




    public function __construct()
    {
        $this->middleware('auth:web');
    }


   public function Create(){

        $e = EnrollmentType::all();
        $c = JobTitle::all();


       return view('dashboard.page.admin.employee.create')->with([
           'e' => $e,
           'c' => $c
       ]);
   }

   public function Store(CreateEmployeeRequest $request){





       $a = new CreateEmployee();
       $a->secure = $this->random_str('alphanum', 20);
       $a->fname = $request->fname;
       $a->mname = $request->mname;
       $a->lname = $request->lname;
       $a->dob = $request->dob;
       $a->enrollment_date = $request->enrollment_date;
       $a->enrollment_id = $request->enrollment_id;
       $a->category = $request->category;
       $a->email = $request->email;
       $a->gender = $request->gender;
       $a->mobile = $request->mobile;
       $a->phone = $request->phone;
       $a->present_address_street = $request->present_address_street;
       $a->present_address_locality = $request->present_address_locality;
       $a->present_address_area = $request->present_address_area;
       $a->present_address_city = $request->present_address_city;
       $a->present_address_district = $request->present_address_district;
       $a->present_address_state = $request->present_address_state;
       $a->present_address_country = $request->present_address_country;
       $a->present_address_pin = $request->present_address_pin;
       $a->present_address_country = $request->present_address_country;
       $a->permanent_address_street = $request->permanent_address_street;
       $a->permanent_address_locality = $request->permanent_address_locality;
       $a->permanent_address_area = $request->permanent_address_area;
       $a->permanent_address_city = $request->permanent_address_city;
       $a->permanent_address_district = $request->permanent_address_district;
       $a->permanent_address_state = $request->permanent_address_state;
       $a->permanent_address_country = $request->permanent_address_country;
       $a->permanent_address_pin = $request->permanent_address_pin;
       $a->password = bcrypt('password');

       $a->save();


       $s = new CreateEmployeeToPicture();
       $s->create_employee_id = $a->id;
       $s->image_location = $request->getHttpHost().'/images/placeholder/user.png';
       $s->image_title = $a->fname;
       $s->save();

       return redirect(route('super.employee.details',$a->id));

   }


   public function editProfile($id, Request $request){

       $a = CreateEmployee::findorfail($id);

       $a->fname = $request->fname;
       $a->mname = $request->mname;
       $a->lname = $request->lname;
       $a->dob = $request->dob;
       $a->enrollment_date = $request->enrollment_date;
       $a->enrollment_id = $request->enrollment_id;
       $a->category = $request->category;
       $a->email = $request->email;
       $a->gender = $request->gender;
       $a->mobile = $request->mobile;
       $a->phone = $request->phone;
       $a->present_address_street = $request->present_address_street;
       $a->present_address_locality = $request->present_address_locality;
       $a->present_address_area = $request->present_address_area;
       $a->present_address_city = $request->present_address_city;
       $a->present_address_district = $request->present_address_district;
       $a->present_address_state = $request->present_address_state;
       $a->present_address_country = $request->present_address_country;
       $a->present_address_pin = $request->present_address_pin;
       $a->present_address_country = $request->present_address_country;
       $a->permanent_address_street = $request->permanent_address_street;
       $a->permanent_address_locality = $request->permanent_address_locality;
       $a->permanent_address_area = $request->permanent_address_area;
       $a->permanent_address_city = $request->permanent_address_city;
       $a->permanent_address_district = $request->permanent_address_district;
       $a->permanent_address_state = $request->permanent_address_state;
       $a->permanent_address_country = $request->permanent_address_country;
       $a->permanent_address_pin = $request->permanent_address_pin;


       $a->save();

       return redirect(route('super.employee.details',$id));


   }



    function random_str($type = 'alphanum', $length = 20)
    {
        switch($type)
        {
            case 'basic'    : return mt_rand();
                break;
            case 'alpha'    :
            case 'alphanum' :
            case 'num'      :
            case 'nozero'   :
                $seedings             = array();
                $seedings['alpha']    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alphanum'] = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['num']      = '0123456789';
                $seedings['nozero']   = '123456789';

                $pool = $seedings[$type];

                $str = '';
                for ($i=0; $i < $length; $i++)
                {
                    $str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
                }
                return $str;
                break;
            case 'unique'   :
            case 'md5'      :
                return md5(uniqid(mt_rand()));
                break;
        }
    }

    public function All(){
       return view('dashboard.page.admin.employee.all');
    }

    public function Details($id){

        $e = EnrollmentType::all();
        $c = JobTitle::all();

       $a = CreateEmployee::findorfail($id);

       return view('dashboard.page.admin.employee.details')->with([
           'employee' => $a,
           'e' => $e,
           'c' => $c

       ]);

    }

    public function Datatable(){
        $invoices = CreateEmployee::select(
            [
                'id',
                'fname',
                'lname',
                'enrollment_id',
                'category',
                'email',
                'mobile',
                'created_at'
            ]);



        return DataTables::of($invoices)
            ->editColumn('created_at', function($invoice) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->toDayDateTimeString();
            })



            ->addColumn('action', function ($invoice) {
               return '<a href="'.route('super.employee.details',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="fa fa-arrow-right"></i>View Details</a>';
            })


            ->rawColumns(['action','delete'])
            ->orderColumn('id', 'created_at $1')
            ->make();

    }



    public function saveAdditional(Request $request){

       $a = new CreateEmployee\CreateEmployeeToAdditionalProfile();

       $a->create_employee_id = $request->create_employee_id;
       $a->basic_salary = $request->basic_salary;
       $a->other_allowance = $request->other_allowance;
       $a->bank_account_no = $request->bank_account_no;
       $a->bank_account_ifsc = $request->bank_account_ifsc;
       $a->bank_account_branch = $request->bank_account_branch;
       $a->bank_account_bank_name = $request->bank_account_bank_name;
       $a->qualification = $request->qualification;
       $a->details = $request->details;
       $a->school = $request->school;
       $a->college = $request->college;
       $a->experience1 = $request->experience1;
       $a->experience2 = $request->experience2;
       $a->experience3 = $request->experience3;
       $a->experience4 = $request->experience4;
       $a->experience5 = $request->experience5;
       $a->reference = $request->reference;
       $a->reference2 = $request->reference2;
       $a->reference3 = $request->reference3;
       $a->reference4 = $request->reference4;
       $a->reference5 = $request->reference5;
       $a->save();

       return redirect()->back();


    }
    public function editsaveAdditional($id, Request $request){

       $a =  CreateEmployee\CreateEmployeeToAdditionalProfile::findorfail($id);

       $a->create_employee_id = $request->create_employee_id;
       $a->basic_salary = $request->basic_salary;
       $a->other_allowance = $request->other_allowance;
       $a->bank_account_no = $request->bank_account_no;
       $a->bank_account_ifsc = $request->bank_account_ifsc;
       $a->bank_account_branch = $request->bank_account_branch;
       $a->bank_account_bank_name = $request->bank_account_bank_name;
       $a->qualification = $request->qualification;
       $a->details = $request->details;
       $a->school = $request->school;
       $a->college = $request->college;
       $a->experience1 = $request->experience1;
       $a->experience2 = $request->experience2;
       $a->experience3 = $request->experience3;
       $a->experience4 = $request->experience4;
       $a->experience5 = $request->experience5;
       $a->reference = $request->reference;
       $a->reference2 = $request->reference2;
       $a->reference3 = $request->reference3;
       $a->reference4 = $request->reference4;
       $a->reference5 = $request->reference5;
       $a->save();

       return redirect()->back();


    }



    public function UploadProfilePicture(Request $request){




        $id = $request->id;
        $imageName1 = time().'.'.$request->image->getClientOriginalExtension();
        $imageName2 = str_replace(' ', '_', $imageName1);
        $imageName = str_replace(' ', '_', $imageName1);
        $image = $request->file('image');
        $t = Storage::disk('public')->put('images/employee/profile/'.$imageName, file_get_contents($image));
        //$imageName = Storage::disk('s3')->url('images/employee/profile/'.$imageName);
        // $imageName3 = Storage::disk('s3')->url('product_image/primary/'.$imageName);

        //  $replacesextraspaces1 = str_replace(' ', '_', $finalimageurl1);

        $location = $request->getHttpHost().'/storage/images/employee/profile/'.$imageName;


       $employee = CreateEmployee::findorfail($id);


       if ($employee->create_employee_to_pictures != null){


           $s = CreateEmployeeToPicture::where('create_employee_id',$id )->first();

           $s->image_location = $location;
           $s->image_title = $employee->fname;

           $s->save();




           return $employee->create_employee_to_pictures;
       } else {

           $s = new CreateEmployeeToPicture();
           $s->create_employee_id = $employee->id;
           $s->image_location = $location;
           $s->image_title = $employee->fname;
           $s->save();

           return redirect()->back();
       }





     //   $s = new ProductImage();
       // $s->images_url = $imageName;
       // $s->extinsion = $imageName2;
       // $s->path = 'product_image/secondary/';
      //  $s->local_path = 'null';
      //  $s->cdn_url = 'https://cdn.emp.tecions.xyz/product_image/secondary/'.$imageName2;
      //  $s->product_id = $id;
      //  $s->name = $imageName2;
      //  $s->save();


    }



    public function Uploaddoccument($id, Request $request){





        $imageName1 = time().'.'.$request->image->getClientOriginalExtension();
        $imageName2 = str_replace(' ', '_', $imageName1);
        $imageName = str_replace(' ', '_', $imageName1);
        $image = $request->file('image');
        $t = Storage::disk('public')->put('images/employee/doccument/'.$imageName, file_get_contents($image));
        //$imageName = Storage::disk('s3')->url('images/employee/profile/'.$imageName);
        // $imageName3 = Storage::disk('s3')->url('product_image/primary/'.$imageName);

        //  $replacesextraspaces1 = str_replace(' ', '_', $finalimageurl1);

        $location = $request->getHttpHost().'/storage/images/employee/doccument/'.$imageName;








           $s = new CreateEmployee\CreateEmployeeToDoccuments();


           $s->create_employee_id = $id;
           $s->name = $request->name;
           $s->description = $request->description;
           $s->location = $location;
           $s->type = $request->name;

           $s->save();




           return back();


    }



    public function RequestDatatable($id){

//   $Requestload = CreateEmployee\CreateEmployeeToRequests::where('create_employee_id',$id)->get();

        $invoices = CreateEmployee\CreateEmployeeToRequests::where('create_employee_id',$id)->select(
            [
                'id',
                'create_employee_id',
                'title',
                'category',
                'description',
                'image_title',
                'image_location',
                'to_request',
                'created_at'
            ]);



        return DataTables::of($invoices)
            ->editColumn('created_at', function($invoice) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->toDayDateTimeString();
            })



            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('super.employee.details',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="fa fa-arrow-right"></i>View Details</a>';
            })


            ->rawColumns(['action','delete'])
            ->orderColumn('id', 'created_at $1')
            ->make();

    }

    public function ChangePassword($id, Request $request){

        $a = CreateEmployee::findorfail($id);

        $a->password = bcrypt($request->password);

        $a->save();

        return redirect(route('super.employee.details',$a->id));



    }


    public function DeleteEmployee($id, Request $request){

        $a = CreateEmployee::findorfail($id);

        $a->delete();

        return redirect(route('super.employee.all'));


    }







}
