<?php

namespace App\Model\Employee;

use Illuminate\Database\Eloquent\Model;

class CreateEmployeeToDailyReport extends Model
{
    public function create_employees()
    {
        return $this->belongsTo('App\Model\Employee\CreateEmployee','create_employee_id');
    }

    public function daily_report_to_images()
    {
        return $this->hasMany('App\Model\Employee\DailyReportToImages','create_employee_to_daily_report_image_id','id');
    }

    public function daily_report_to_files()
    {
        return $this->hasMany('App\Model\Employee\DailyReportToFile','create_employee_to_daily_report_image_id','id');
    }
}
