<?php

namespace App\Model\Employee;

use Illuminate\Database\Eloquent\Model;

class CreateEmployeeToTranaction extends Model
{
    public function create_employees()
    {
        return $this->belongsTo('App\Model\Employee\CreateEmployee','create_employee_id');
    }
}
