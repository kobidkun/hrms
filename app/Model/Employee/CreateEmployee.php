<?php

namespace App\Model\Employee;


use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;



class CreateEmployee extends Authenticatable
{




    use Notifiable, HasApiTokens;



    protected $fillable = [
        'mobile',
        'email',
        'password',
    ];

    protected $hidden = [
        'password'
    ];



    public function create_employee_to_activity_details()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToActivityDetails','create_employee_id');
    }

    public function create_employee_to_additional_profiles()
    {
        return $this->hasOne('App\Model\Employee\CreateEmployeeToAdditionalProfile','create_employee_id');
    }

    public function create_employee_to_assets()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToAsset','create_employee_id');
    }

    public function create_employee_to_attendences()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToAttendence','create_employee_id');
    }

    public function create_employee_to_complains()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToComplain','create_employee_id');
    }

    public function create_employee_to_daily_reports()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToDailyReport','create_employee_id');
    }

    public function create_employee_to_direct_messages()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToDirectMessage','create_employee_id');
    }

    public function create_employee_to_doccuments()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToDoccuments','create_employee_id');
    }

    public function create_employee_to_expences()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToExpences','create_employee_id');
    }

    public function create_employee_to_leads()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToLeads','create_employee_id');
    }

    public function create_employee_to_leave_applications()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToLeaveApplication','create_employee_id');
    }

    public function create_employee_to_live_locations()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToLiveLocation','create_employee_id');
    }

    public function create_employee_to_payrolls()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToPayroll','create_employee_id');
    }

    public function create_employee_to_pictures()
    {
        return $this->hasOne('App\Model\Employee\CreateEmployeeToPicture','create_employee_id');
    }

    public function create_employee_to_requests()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToRequests','create_employee_id');
    }

    public function create_employee_to_tranactions()
    {
        return $this->hasMany('App\Model\Employee\CreateEmployeeToTranaction','create_employee_id');
    }

    public function employee_to_track_login_logouts()
    {
        return $this->hasMany('App\Model\Employee\EmployeeToTrackLoginLogout','create_employee_id');
    }




}
