<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class JobTitle extends Model
{
    public function tracking_masters()
    {
        return $this->hasOne('App\Model\Master\TrackingMaster','category_id');
    }
}
