<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
            <span>Main</span>
            <hr/>
        </li>
        {{--<li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="ti-dashboard mr-20"></i>
                    <span class="right-nav-text">Dashboard</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="dashboard_dr" class="collapse collapse-level-1">
                <li>
                    <a href="index.html">Dashboard</a>
                </li>
                <li>
                    <a href="index2.html"><div class="pull-left"><span>Report</span></div><div class="pull-right"><span class="label label-success">Hot</span></div><div class="clearfix"></div></a>
                </li>
                <li>
                    <a href="profile.html">Perfomance Anaylisis</a>
                </li>
            </ul>
        </li>--}}
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_dr"><div class="pull-left"><i class="ti-receipt  mr-20"></i>
                    <span class="right-nav-text">Daily Report</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="ecom_dr" class="collapse collapse-level-1">
                <li>
                    <a href="{{route('employee.dashboard')}}">View Daily Report</a>
                </li>
                <li>
                    <a href="{{route('employee.daily.report.create')}}">Create Daily Report</a>
                </li>
                <li>
                    <a href="#">Your Perfomance</a>
                </li>

            </ul>
        </li>


        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_dr"><div class="pull-left"><i class="ti-notepad  mr-20"></i>
                    <span class="right-nav-text">Notices & Announcement</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="ecom_dr" class="collapse collapse-level-1">
                <li>
                    <a href="#">Latest Notice</a>
                </li>
                <li>
                    <a href="#">Latest Announcements</a>
                </li>
                <li>
                    <a href="#">Your Perfomance</a>
                </li>

            </ul>
        </li>


        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_dr"><div class="pull-left"><i class="ti-user  mr-20"></i>
                    <span class="right-nav-text">Your Profile</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="ecom_dr" class="collapse collapse-level-1">
                <li>
                    <a href="#">View Profile</a>
                </li>


            </ul>
        </li>

        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_dr"><div class="pull-left"><i class="ti-money  mr-20"></i>
                    <span class="right-nav-text">Expense</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="ecom_dr" class="collapse collapse-level-1">
                <li>
                    <a href="#">Add New Expense</a>
                </li>

                <li>
                    <a href="#">All Expense</a>
                </li>


            </ul>
        </li>


        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_dr"><div class="pull-left"><i class="fa fa-rupee  mr-20"></i>
                    <span class="right-nav-text">MY Salary</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="ecom_dr" class="collapse collapse-level-1">
                <li>
                    <a href="#">My Salary </a>
                </li>


            </ul>
        </li>


        <li>
            <a href="{{route('employee.logout.get')}}" ><div class="pull-left"><i class="ti-lock  mr-20"></i>
                    <span class="right-nav-text">Logout</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>

        </li>




       {{-- <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#app_dr"><div class="pull-left"><i class="ti-image mr-20"></i><span class="right-nav-text">Apps </span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="app_dr" class="collapse collapse-level-1">
                <li>
                    <a href="chats.html">chats</a>
                </li>
                <li>
                    <a href="calendar.html">calendar</a>
                </li>
                <li>
                    <a href="weather.html">weather</a>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#email_dr">Email<div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
                    <ul id="email_dr" class="collapse collapse-level-2">
                        <li>
                            <a href="inbox.html">inbox</a>
                        </li>
                        <li>
                            <a href="inbox-detail.html">detail email</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#contact_dr">Contacts<div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
                    <ul id="contact_dr" class="collapse collapse-level-2">
                        <li>
                            <a href="contact-list.html">list</a>
                        </li>
                        <li>
                            <a href="contact-card.html">cards</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="file-manager.html">File Manager</a>
                </li>
                <li>
                    <a href="todo-tasklist.html">To Do/Tasklist</a>
                </li>
            </ul>
        </li>
        <li class="navigation-header mt-20">
            <span>component</span>
            <hr/>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr"><div class="pull-left"><i class="ti-pencil-alt  mr-20"></i><span class="right-nav-text">UI Elements</span></div><div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
            <ul id="ui_dr" class="collapse collapse-level-1 two-col-list">
                <li>
                    <a href="panels-wells.html">Panels & Wells</a>
                </li>
                <li>
                    <a href="modals.html">Modals</a>
                </li>
                <li>
                    <a href="sweetalert.html">Sweet Alerts</a>
                </li>
                <li>
                    <a href="notifications.html">notifications</a>
                </li>
                <li>
                    <a href="typography.html">Typography</a>
                </li>
                <li>
                    <a href="buttons.html">Buttons</a>
                </li>
                <li>
                    <a href="accordion-toggle.html">Accordion / Toggles</a>
                </li>
                <li>
                    <a href="tabs.html">Tabs</a>
                </li>
                <li>
                    <a href="progressbars.html">Progress bars</a>
                </li>
                <li>
                    <a href="skills-counter.html">Skills & Counters</a>
                </li>
                <li>
                    <a href="pricing.html">Pricing Tables</a>
                </li>
                <li>
                    <a href="nestable.html">Nestables</a>
                </li>
                <li>
                    <a href="dorpdown.html">Dropdowns</a>
                </li>
                <li>
                    <a href="bootstrap-treeview.html">Tree View</a>
                </li>
                <li>
                    <a href="carousel.html">Carousel</a>
                </li>
                <li>
                    <a href="range-slider.html">Range Slider</a>
                </li>
                <li>
                    <a href="grid-styles.html">Grid</a>
                </li>
                <li>
                    <a href="bootstrap-ui.html">Bootstrap UI</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#comp_dr"><div class="pull-left"><i class="ti-check-box  mr-20"></i><span class="right-nav-text">Components</span></div><div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
            <ul id="comp_dr" class="collapse collapse-level-1">
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#form_dr"><div class="pull-left"><span class="right-nav-text">Forms</span></div><div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
                    <ul id="form_dr" class="collapse collapse-level-2 two-col-list dr-change-pos">
                        <li>
                            <a href="form-element.html">Basic Forms</a>
                        </li>
                        <li>
                            <a href="form-layout.html">form Layout</a>
                        </li>
                        <li>
                            <a href="form-advanced.html">Form Advanced</a>
                        </li>
                        <li>
                            <a href="form-mask.html">Form Mask</a>
                        </li>
                        <li>
                            <a href="form-picker.html">Form Picker</a>
                        </li>
                        <li>
                            <a href="form-validation.html">form Validation</a>
                        </li>
                        <li>
                            <a href="form-wizard.html">Form Wizard</a>
                        </li>
                        <li>
                            <a href="form-x-editable.html">X-Editable</a>
                        </li>
                        <li>
                            <a href="cropperjs.html">Cropperjs</a>
                        </li>
                        <li>
                            <a href="form-file-upload.html">File Upload</a>
                        </li>
                        <li>
                            <a href="dropzone.html">Dropzone</a>
                        </li>
                        <li>
                            <a href="bootstrap-wysihtml5.html">Bootstrap Wysihtml5</a>
                        </li>
                        <li>
                            <a href="tinymce-wysihtml5.html">Tinymce Wysihtml5</a>
                        </li>
                        <li>
                            <a href="summernote-wysihtml5.html">summernote</a>
                        </li>
                        <li>
                            <a href="typeahead-js.html">typeahead</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#chart_dr"><div class="pull-left"><span class="right-nav-text">Charts </span></div><div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
                    <ul id="chart_dr" class="collapse collapse-level-2 dr-change-pos">
                        <li>
                            <a href="flot-chart.html">Flot Chart</a>
                        </li>
                        <li>
                            <a href="echart.html">Echart Chart</a>
                        </li>
                        <li>
                            <a href="morris-chart.html">Morris Chart</a>
                        </li>
                        <li>
                            <a href="chart.js.html">chartjs</a>
                        </li>
                        <li>
                            <a href="chartist.html">chartist</a>
                        </li>
                        <li>
                            <a href="easy-pie-chart.html">Easy Pie Chart</a>
                        </li>
                        <li>
                            <a href="sparkline.html">Sparkline</a>
                        </li>
                        <li>
                            <a href="peity-chart.html">Peity Chart</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#table_dr"><div class="pull-left"><span class="right-nav-text">Tables</span></div><div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
                    <ul id="table_dr" class="collapse collapse-level-2 dr-change-pos">
                        <li>
                            <a href="basic-table.html">Basic Table</a>
                        </li>
                        <li>
                            <a href="bootstrap-table.html">Bootstrap Table</a>
                        </li>
                        <li>
                            <a href="data-table.html">Data Table</a>
                        </li>
                        <li>
                            <a href="export-table.html">Export Table</a>
                        </li>
                        <li>
                            <a href="responsive-data-table.html">RSPV DataTable</a>
                        </li>
                        <li>
                            <a href="responsive-table.html">Responsive Table</a>
                        </li>
                        <li>
                            <a href="editable-table.html">Editable Table</a>
                        </li>
                        <li>
                            <a href="foo-table.html">Foo Table</a>
                        </li>
                        <li>
                            <a href="jsgrid-table.html">Jsgrid Table</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#icon_dr"><div class="pull-left"><span class="right-nav-text">Icons</span></div><div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
                    <ul id="icon_dr" class="collapse collapse-level-2 dr-change-pos">
                        <li>
                            <a href="fontawesome.html">Fontawesome</a>
                        </li>
                        <li>
                            <a href="themify.html">Themify</a>
                        </li>
                        <li>
                            <a href="linea-icon.html">Linea</a>
                        </li>
                        <li>
                            <a href="simple-line-icons.html">Simple Line</a>
                        </li>
                        <li>
                            <a href="pe-icon-7.html">Pe-icon-7</a>
                        </li>
                        <li>
                            <a href="glyphicons.html">Glyphicons</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#maps_dr"><div class="pull-left"><span class="right-nav-text">maps</span></div><div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
                    <ul id="maps_dr" class="collapse collapse-level-2 dr-change-pos">
                        <li>
                            <a href="vector-map.html">Vector Map</a>
                        </li>
                        <li>
                            <a href="google-map.html">Google Map</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="navigation-header mt-20">
            <span>featured</span>
            <hr/>
        </li>
        <li>
            <a class="active" href="javascript:void(0);" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="ti-shield mr-20"></i><span class="right-nav-text">Pages</span></div><div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
            <ul id="pages_dr" class="collapse collapse-level-1">
                <li>
                    <a class="active-page" href="blank.html">Blank Page</a>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#auth_dr">Authantication pages<div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
                    <ul id="auth_dr" class="collapse collapse-level-2 dr-change-pos">
                        <li>
                            <a href="login.html">Login</a>
                        </li>
                        <li>
                            <a href="signup.html">Register</a>
                        </li>
                        <li>
                            <a href="forgot-password.html">Recover Password</a>
                        </li>
                        <li>
                            <a href="reset-password.html">reset Password</a>
                        </li>
                        <li>
                            <a href="locked.html">Lock Screen</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#invoice_dr">Invoice<div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
                    <ul id="invoice_dr" class="collapse collapse-level-2 dr-change-pos">
                        <li>
                            <a href="invoice.html">Invoice</a>
                        </li>
                        <li>
                            <a href="invoice-archive.html">Invoice Archive</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="collapse" data-target="#error_dr">error pages<div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
                    <ul id="error_dr" class="collapse collapse-level-2 dr-change-pos">
                        <li>
                            <a href="404.html">Error 404</a>
                        </li>
                        <li>
                            <a href="500.html">Error 500</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="gallery.html">Gallery</a>
                </li>
                <li>
                    <a href="timeline.html">Timeline</a>
                </li>
                <li>
                    <a href="faq.html">FAQ</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="documentation.html"><div class="pull-left"><i class="ti-book mr-20"></i><span class="right-nav-text">documentation</span></div><div class="clearfix"></div></a>
        </li>--}}
    </ul>
</div>