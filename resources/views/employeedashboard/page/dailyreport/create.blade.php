@extends('employeedashboard.base')


@section('contents')



    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Create Daily Report</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Dashboard</a></li>
                <li><a href="#"><span>Employee</span></a></li>
                <li class="active"><span>Create new daily Report</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>




    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">


                        <div class="row">



                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif


                                    <form
                                            role="form" method="post"
                                            action="{{ route('employee.daily.report.save') }}"
                                    >


                                        {{ csrf_field() }}

                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-receipt mr-10"></i>Daily Report</h6>
                                            <hr class="light-grey-hr"/>


                                            <input type="hidden" class="lang" name="lang" >
                                            <input type="hidden" class="long" name="long" >

                                            <div class="row">



                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Title</label>
                                                        <input type="text" required name="title" class="form-control" placeholder=" ">

                                                    </div>
                                                </div>



                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Description</label>
                                                        <textarea type="text" required name="description" class="form-control" cols="5"></textarea>

                                                    </div>
                                                </div>

                                                <p class="text-center">**On next Page you can Attach Images or Files which you want to submit</p>



                                                <div class="col-md-12">




                                                        <button id="find_btn" class="location-button btn btn-block btn-info  mr-10">
                                                            Find my Location
                                                        </button>

                                                    <br>
                                                    <br>


                                                 <h4 style="text-align: center"> <div id="result"></div></h4>
                                                </div>




                                                <!--/span-->













                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->































                                        </div>










                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-block btn-success  mr-10 hidden  submitbutton"> Save</button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('scripts')

    <link href="{{asset('/static/dist/js/framework/bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css">

    <script src="{{asset('/static/dist/js/framework/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        if ("geolocation" in navigator){ //check geolocation available
            //try to get user current location using getCurrentPosition() method
            navigator.geolocation.getCurrentPosition(function(position){

                $('.long').val(position.coords.longitude);
                $('.lang').val(position.coords.latitude);

                $("#result").html("found your Location");

                $( ".submitbutton" ).removeClass( "hidden" );
                $( ".location-button " ).addClass( "hidden-lg" );

                alert("We have Successfully found your Location");
            });
        }else{
            alert("We coundnot get your location please click find location");
        }
    </script>
    <script>


        $("#find_btn").click(function (event) { //user clicks button
            event.preventDefault();
            if ("geolocation" in navigator){ //check geolocation available
                //try to get user current location using getCurrentPosition() method
                navigator.geolocation.getCurrentPosition(function(position){

                    $('.long').val(position.coords.longitude);
                    $('.lang').val(position.coords.latitude);

                    $("#result").html("found your Location");

                    $( ".submitbutton" ).removeClass( "hidden" );
                    $( ".location-button " ).addClass( "hidden-lg" );
                });
            }else{
                alert("Browser doesn't support geolocation! Please use App");
            }
        });





    </script>


@endsection