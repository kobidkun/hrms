@extends('employeedashboard.base')


@section('contents')



    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Daily report Details</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Dashboard</a></li>
                <li><a href="#"><span>Employee</span></a></li>
                <li class="active"><span>Daily Upload  Details</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>







    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-md-12">
                                <div class="product-detail-wrap">



                                    <h3 class="mb-20 weight-500"></h3>


                                    <div class="product-price head-font mb-30"><span style="color: #000000"> {{$r->title}}</span> </div>
                                    <div class="product-price head-font mb-30">
                                        <p>{{$r->description}}</p>
                                    </div>







                                    <div class="product-price head-font mb-30"><span style="color: #000000">Date Time: {{$r->created_at}}</span></div>









                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->

    <!-- Row -->




    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Uploaded Images</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div  class="panel-body">




                       <div class="col-sm-12 mt-40">

                            <div class="row ">
                                @foreach($imgs as $a)

                                        <div class="col-xs-6 col-sm-4">


                                            <img src="http://{{$a->image_location}}" width="250px" alt=""/>

                                            <br>
                                            {{$a->created_at}}







                                    </div>

                                @endforeach





                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>








    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">


                <div class="panel-wrapper collapse in">
                    <div class="panel-body">












                        <form method="POST" action="{{ route('employee.daily.report.image.upload',$r->id) }}"
                              class="m-dropzone dropzone m-dropzone--primary"
                              id="mDropzoneTwoPrimary"
                        >

                            {{ csrf_field() }}
                            <input name="id" value="{{$r->create_employee_id}}" type="hidden">
                            <div class="m-dropzone__msg dz-message needsclick">
                                <h3 class="m-dropzone__msg-title">
                                    Drop  Image here or click to upload.
                                </h3>
                                <span class="m-dropzone__msg-desc">
														MAX Size 1 MB
													</span>
                            </div>
                        </form>







                    </div>
                </div>





            </div>
        </div>
    </div>





    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Uploaded Files</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div  class="panel-body">




                        <div class="col-sm-12 mt-40">

                            <div class="row ">
                                @foreach($files as $a)

                                    <div class="col-xs-6 col-sm-3">



                                        <a href="http://{{$a->file_location}}" class="btn btn-primary">


                                            {{$a->file_title}}
                                        </a>


                                        <br>
                                        {{$a->created_at}}







                                    </div>

                                @endforeach





                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">


                <div class="panel-wrapper collapse in">
                    <div class="panel-body">












                        <form method="POST" action="{{ route('web.employee.daily.report.files.upload',$r->id) }}"
                              class="m-dropzone dropzone m-dropzone--primary"
                              id="employeefilesupload"
                        >

                            {{ csrf_field() }}
                            <input name="id" value="{{$r->create_employee_id}}" type="hidden">
                            <div class="m-dropzone__msg dz-message needsclick">
                                <h3 class="m-dropzone__msg-title">
                                    Drop  Files  here or click to upload.
                                </h3>
                                <span class="m-dropzone__msg-desc">
														Only Docs, PDF, XLS, CSV
													</span>
                            </div>
                        </form>








                    </div>
                </div>





            </div>
        </div>
    </div>











@endsection




@section('scripts')









    <script src="{{asset('static/dist/js/dropzone.js')}}"></script>
    <script>
        //== Class definition

        var DropzoneDemo = function () {
            //== Private functions
            var demos = function () {
                // single file upload
                // multiple file upload
                Dropzone.options.mDropzoneTwoPrimary = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 10, // MB
                    maxFiles: 10,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif"

                };

                Dropzone.options.mDropzoneTwo = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 5, // MB
                    maxFiles: 20,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif"

                };
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        DropzoneDemo.init();
    </script>



    <script>
        //== Class definition

        var DropzoneDemo = function () {
            //== Private functions
            var demos = function () {
                // single file upload
                // multiple file upload
                Dropzone.options.employeefilesupload = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 10, // MB
                    maxFiles: 10,
                    acceptedFiles: ".xls,.csv,.pdf,.doc,.ppt,.docx"

                };


            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        DropzoneDemo.init();
    </script>






@endsection



@section('styles')


    <link href="{{asset('static/dist/css/dropzone.css')}}" rel="stylesheet" type="text/css"/>


@endsection