
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>HRMS - HUMAN RESOURCE MANAGEMENT SYSTEM</title>
    <meta name="description" content="HUMAN RESOURCE MANAGEMENT SYSTEM." />
    <meta name="keywords" content=" HUMAN RESOURCE MANAGEMENT SYSTEM" />
    <meta name="author" content="TECIONS"/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Data table CSS -->
    <!-- Custom CSS -->
    <link href="{{asset('/css/style.min.css')}}" rel="stylesheet" type="text/css">
    @yield('styles')
</head>

<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->
<div class="wrapper box-layout theme-2-active navbar-top-navyblue horizontal-nav">
    <!-- Top Menu Items -->
       @include('employeedashboard.components.topmenu')
    <!-- /Top Menu Items -->

    <!-- Left Sidebar Menu -->
@include('employeedashboard.components.leftmenu')
    <!-- /Left Sidebar Menu -->

    <!-- Right Sidebar Menu -->
@include('employeedashboard.components.rightmenu')
    <!-- /Right Sidebar Menu -->



    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->







            @yield('contents')










            <!-- /Title -->

            <!-- Footer -->
            <footer class="footer container-fluid pl-30 pr-30">
                <div class="row">
                    <div class="col-sm-12">
                        <p>2018 &copy; Tecions.</p>
                    </div>
                </div>
            </footer>
            <!-- /Footer -->
        </div>
    </div>
    <!-- /Main Content -->





</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="{{asset('static/dist/js/framework/jquery.min.js')}}"></script>
<script src="{{asset('static/dist/js/framework/bootstrap.min.js')}}"></script>
<script src="{{asset('static/dist/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('static/dist/js/framework/owl.carousel.min.js')}}"></script>
<script src="{{asset('static/dist/js/framework/switchery.min.js')}}"></script>
<script src="{{asset('static/dist/js/framework/dropdown-bootstrap-extended.js')}}"></script>
<script src="{{asset('static/dist/js/init.js')}}"></script>

@yield('scripts')



</body>

</html>
