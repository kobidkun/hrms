@extends('dashboard.base')


@section('contents')



    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Create New Employee</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="index.html">Dashboard</a></li>
                <li><a href="#"><span>Employee</span></a></li>
                <li class="active"><span>Create New Employee</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>




    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">with two columns</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">


                        <div class="row">



                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">
                                    <form action="#">




                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account mr-10"></i>Employee's Info</h6>
                                            <hr class="light-grey-hr"/>



                                            <div class="row">



                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">First Name</label>
                                                        <input type="text" name="fname" class="form-control" placeholder="Amit">

                                                    </div>
                                                </div>




                                                <!--/span-->



                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Middle Name</label>
                                                        <input type="text"  name="mname" class="form-control" placeholder="Kumar">

                                                    </div>
                                                </div>



                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label mb-10">Last Name</label>
                                                        <input type="text" name="lname"  class="form-control" placeholder="Jain">

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label mb-10">Mobile</label>
                                                        <input type="text" name="mobile"  class="form-control" placeholder="Mobile">

                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label mb-10">Emergency Contact</label>
                                                        <input type="text" name="phone"  class="form-control" placeholder="Emergency Contact">

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label mb-10">Email</label>
                                                        <input type="email" name="email"   class="form-control" placeholder="Email">

                                                    </div>
                                                </div>






                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->









                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Gender</label>
                                                        <select name="gender" class="form-control">
                                                            <option value="">Male</option>
                                                            <option value="">Female</option>
                                                        </select>
                                                        <span class="help-block"> Select Employee gender </span>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Date of Birth</label>

                                                        <div class='input-group date' id='birthdaydate'>
                                                            <input name="dob"  type='text' value="01-01-1980" class="form-control" />
                                                            <span class="input-group-addon">
																	<span class="fa fa-calendar"></span>
																</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>




                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Enrollment Date</label>
                                                        <div class='input-group date' id='enrollment'>
                                                            <input name="enrollment_date" type='text' value="" class="form-control" />
                                                            <span class="input-group-addon">
																	<span class="fa fa-calendar"></span>
																</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Category</label>
                                                        <select name="category" class="form-control">
                                                            <option value="">Sales</option>
                                                            <option value="">Marketing</option>
                                                            <option value="">Backoffice</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>





                                            <!-- /Row -->

                                            <!-- /Row -->

                                            <div class="seprator-block"></div>

                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>Local Address</h6>
                                            <hr class="light-grey-hr"/>
                                            <div class="row">


                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Street</label>
                                                        <input name="present_address_street" type="text" class="form-control">
                                                    </div>
                                                </div>


                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Locality</label>
                                                        <input name="present_address_locality"  type="text" class="form-control">
                                                    </div>
                                                </div>


                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Area</label>
                                                        <input name="present_address_area" type="text" class="form-control">
                                                    </div>
                                                </div>



                                            </div>



                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">City</label>
                                                        <input name="present_address_city" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">State</label>
                                                        <input name="present_address_state" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>



                                            <!-- /Row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Post Code</label>
                                                        <input name="present_address_pin" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Country</label>
                                                        <select name="present_address_country" class="form-control">
                                                            <option>--Select your Country--</option>
                                                            <option>India</option>
                                                            <option>Nepal</option>
                                                            <option>Bhutan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>












                                            <div class="seprator-block"></div>

                                            <div class="col-md-8 ">
                                                <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>

                                                    Permanent  Address

                                                </h6>
                                            </div>

                                            <div class="col-md-4 ">

                                                <button class="btn btn-success btn-outline btn-icon right-icon"><span>Copy From Present Address</span><i class="fa fa-paste"></i></button>


                                            </div>


                                            <hr class="light-grey-hr"/>
                                            <div class="row">


                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Street</label>
                                                        <input name="permanent_address_street"  type="text" class="form-control">
                                                    </div>
                                                </div>


                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Locality</label>
                                                        <input name="permanent_address_locality"  type="text" class="form-control">
                                                    </div>
                                                </div>


                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Area</label>
                                                        <input name="permanent_address_area"  type="text" class="form-control">
                                                    </div>
                                                </div>



                                            </div>



                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">City</label>
                                                        <input name="permanent_address_city" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">State</label>
                                                        <input name="permanent_address_state" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>



                                            <!-- /Row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Post Code</label>
                                                        <input name="permanent_address_pin" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Country</label>
                                                        <select name="permanent_address_country" class="form-control">
                                                            <option>--Select your Country--</option>
                                                            <option>India</option>
                                                            <option>Nepal</option>
                                                            <option>Bhutan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>





                                        </div>










                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                            <button type="button" class="btn btn-default">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @endsection


@section('scripts')

    <link href="{{asset('/static/dist/js/framework/bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css">

    <script src="{{asset('/static/dist/js/framework/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js')}}"></script>

    <script>
        $('#birthdaydate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose:true,
            todayHighlight:true
        });

        $('#enrollment').datepicker({
            format: 'dd/mm/yyyy',
            autoclose:true,
            todayHighlight:true
        });
    </script>


    @endsection