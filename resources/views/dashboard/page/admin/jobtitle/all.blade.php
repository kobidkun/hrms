@extends('dashboard.base')


@section('contents')



    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">

            <a class="btn btn-primary" href="{{route('super.job.master.create')}}">Create Job Titles</a>

        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Dashboard</a></li>
                <li><a href="#"><span>Employee</span></a></li>
                <li class="active"><span>All Job Titles</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>







                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-default card-view">
                                    <div class="panel-heading">
                                        <div class="pull-left">
                                            <h6 class="panel-title txt-dark">Job Titles</h6>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <div class="table-wrap">
                                                <div class="table-responsive">





                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col">Title</th>
                                                            <th scope="col">Description</th>
                                                            <th scope="col">Delete</th>
                                                            <th scope="col">Edit</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>


                                                        @foreach($a as $j)
                                                            <tr>
                                                                <th scope="row">1</th>
                                                                <td>{{$j->name}}</td>
                                                                <td>{{$j->description}}</td>
                                                                <td>

                                                                    <a class="btn btn-danger" href="{{route('super.job.master.delete',$j->id)}}">
                                                                        Delete
                                                                    </a>


                                                                </td>

                                                                <td>

                                                                    <a class="btn btn-primary" href="{{route('super.job.master.titles.edit',$j->id)}}">
                                                                        Edit
                                                                    </a>


                                                                </td>
                                                            </tr>

                                                        @endforeach

                                                        </tbody>
                                                    </table>









                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>








@endsection




@section('scripts')

    <script src="{{asset('/static/dist/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $(function() {
            $('#datable_2').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('super.employee.datatable')}}',
                order: [ [0, 'desc'] ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'fname', name: 'fname' },
                    { data: 'lname', name: 'lname' },
                    { data: 'enrollment_id', name: 'enrollment_id' },
                    { data: 'category', name: 'category' },
                    { data: 'email', name: 'email' },
                    { data: 'mobile', name: 'mobile' },
                   {data: 'action', name: 'action', orderable: false, searchable: false},

                ]
            });

        });
    </script>


@endsection


@section('styles')

    <link href="{{asset('/static/dist/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>


@endsection