@extends('dashboard.base')


@section('contents')



    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Create Job Title</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Dashboard</a></li>
                <li><a href="#"><span>Job Title</span></a></li>
                <li class="active"><span>{{$d->name}}</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>




    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">


                        <div class="row">



                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif


                                        <form
                                              role="form" method="post"
                                              action="{{ route('super.job.titles.master.update',$d->id) }}"
                                        >


                                        {{ csrf_field() }}

                                        <div class="form-body">

                                            <hr class="light-grey-hr"/>



                                            <div class="row">



                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Title</label>
                                                        <input type="text" value="{{$d->name}}" name="name" required class="form-control title" placeholder="Title">

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">TTL</label>
                                                        <input type="text" value="{{$d->slug}}" name="slug" placeholder="Time to Live"  required class="form-control slug">

                                                    </div>
                                                </div>




                                                <!--/span-->



                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Description</label>
                                                        <input type="text"  value="{{$d->description}}" name="description" class="form-control" placeholder="Description">

                                                    </div>
                                                </div>






                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Location Provider</label>
                                                        <select name="location_provider">
                                                            <option value="DISTANCE_FILTER_PROVIDER">DISTANCE FILTER PROVIDER</option>
                                                            <option value="ACTIVITY_PROVIDER">ACTIVITY PROVIDER</option>
                                                            <option value="RAW_PROVIDER">RAW PROVIDER</option>
                                                        </select>

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Accuracy</label>
                                                        <select name="accuracy">
                                                            <option value="HIGH_ACCURACY">HIGH ACCURACY</option>
                                                            <option value="MEDIUM_ACCURACY">MEDIUM_ACCURACY</option>
                                                            <option value="LOW_ACCURACY">LOW ACCURACY</option>
                                                            <option value="PASSIVE_ACCURACY">PASSIVE ACCURACY</option>
                                                        </select>

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Stationary Radius</label>
                                                        <input type="number" value="50"  name="stationary_radius" class="form-control"
                                                               placeholder="Stationary Radius">

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Debug</label>
                                                        <select name="debug">
                                                            <option value="false">False</option>
                                                            <option value="true">True</option>
                                                        </select>

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Distance Filter</label>
                                                        <input type="number" value="500"  name="distance_filter" class="form-control"
                                                               placeholder="Distance Filter">

                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Interval</label>
                                                        <input type="number" value="6000" name="interval" class="form-control"
                                                               placeholder="Interval">

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Fastest Interval</label>
                                                        <input type="number" value="1200"  name="fastest_interval" class="form-control"
                                                               placeholder="Interval">

                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Activity Interval</label>
                                                        <input type="number" value="10000" name="activity_interval" class="form-control"
                                                               placeholder="Interval">

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Stop on Still Activity</label>
                                                        <select name="stop_still">
                                                            <option value="true">True</option>
                                                            <option value="false">False</option>
                                                        </select>

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Start Foreground</label>
                                                        <select name="start_foreground">
                                                            <option value="true">True</option>
                                                            <option value="false">False</option>
                                                        </select>

                                                    </div>
                                                </div>





                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->





                                        </div>










                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>







                        <br>
                        <br>
                        <br>
                        <br>

                        <div class="row">

                            <h3 class="center-block"><center>Lern More</center></h3>

                            <br>
                            <br>
                            <br>
                            <div class="col-sm-4 col-xs-4">

                                <h4>Location providers</h4>

                                <h6>DISTANCE_FILTER_PROVIDER</h6>

                                <p>This is classic provider, originally from cristocracy. It's best to use this one as background location provider. It is using Stationary API and elastic distance filter to achieve optimal battery and data usage</p>


                                <br>
                                <br>

                                <h6>ACTIVITY_PROVIDER (Android only)</h6>
                                <p>
                                    This one is best to use as foreground location provider (but works in background as well). It uses Android FusedLocationProviderApi and ActivityRecognitionApi for maximum battery saving. This provider is alternative to w3c window.navigator.watchPosition, but you're in control how often should updates be polled from GPS. Slower updates means lower battery consumption. You can adjust position update interval by settings options interval and fastestInterval. Option fastestInterval is used, when there are other apps asking for positions. In that case your app can be updated more often and fastestInterval is the upper limit of how fast can your app process location updates. Option activitiesInterval specifies how often activity recognition occurs. Larger values will result in fewer activity detections while improving battery life. Smaller values will result in more frequent activity detections but will consume more power since the device must be woken up more frequently


                                </p>

                                <br>
                                <br>

                                <h6>RAW_PROVIDER</h6>

                                <p>This provider doesn't do any location processing, but rather returns locations as recorded by device sensors.

                                </p>


                            </div>

                            <div class="col-sm-4 col-xs-4">

                                <h6>Desired Accuracy</h6>

                                <p>Desired accuracy in meters. Possible values [HIGH_ACCURACY, MEDIUM_ACCURACY, LOW_ACCURACY, PASSIVE_ACCURACY]. Accuracy has direct effect on power drain. Lower accuracy = lower power drain</p>

                            </div>


                            <div class="col-sm-4 col-xs-4">

                                <h6>Stationary Radius</h6>

                                <p>
                                    Stationary radius in meters. When stopped, the minimum distance the device must move beyond the stationary location for aggressive background-tracking to engage.

                                </p>

                            </div>


                            <div class="col-sm-4 col-xs-4">

                                <h6>Distance Filter</h6>

                                <p>
                                    The minimum distance (measured in meters) a device must move horizontally before an update event is generated.
                                </p>

                            </div>

                            <br>
                            <br>


                            <div class="col-sm-4 col-xs-4">

                                <h6>Interval</h6>

                                <p>
                                    The minimum time interval between location updates in milliseconds.

                                </p>

                            </div>


                            <div class="col-sm-4 col-xs-4">

                                <h6>Fastest Interval</h6>

                                <p>
                                    Fastest rate in milliseconds at which your app can handle location updates

                                </p>

                            </div>



                            <div class="col-sm-4 col-xs-4">

                                <h6>Activity Interval</h6>

                                <p>
                                    Rate in milliseconds at which activity recognition occurs. Larger values will result in fewer activity detections while improving battery life

                                </p>

                            </div>







                        </div>









                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('scripts')

    <script>






    </script>


@endsection