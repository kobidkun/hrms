@extends('dashboard.base')


@section('contents')



    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">All Employee</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="index.html">Dashboard</a></li>
                <li><a href="#"><span>Employee</span></a></li>
                <li class="active"><span>All Employee</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>







                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-default card-view">
                                    <div class="panel-heading">
                                        <div class="pull-left">
                                            <h6 class="panel-title txt-dark">Employee</h6>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <div class="table-wrap">
                                                <div class="table-responsive">
                                                    <table id="datable_2" class="table table-hover table-bordered display mb-30" >


                                                        <thead>

                                                        <tr>
                                                            <th>Sno</th>
                                                            <th>Fname</th>
                                                            <th>Lname</th>
                                                            <th>Enrollment Id</th>
                                                            <th>Category</th>
                                                            <th>Email</th>
                                                            <th>Mobile</th>
                                                            <th>Details</th>


                                                        </tr>
                                                        </thead>




                                                        <tfoot>
                                                        <tr>
                                                            <th>Sno</th>
                                                            <th>Fname</th>
                                                            <th>Lname</th>
                                                            <th>Enrollment Id</th>
                                                            <th>Category</th>
                                                            <th>Email</th>
                                                            <th>Mobile</th>
                                                            <th>Details</th>

                                                        </tr>
                                                        </tfoot>



                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>








@endsection




@section('scripts')

    <script src="{{asset('/static/dist/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $(function() {
            $('#datable_2').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('super.employee.datatable')}}',
                order: [ [0, 'desc'] ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'fname', name: 'fname' },
                    { data: 'lname', name: 'lname' },
                    { data: 'enrollment_id', name: 'enrollment_id' },
                    { data: 'category', name: 'category' },
                    { data: 'email', name: 'email' },
                    { data: 'mobile', name: 'mobile' },
                   {data: 'action', name: 'action', orderable: false, searchable: false},

                ]
            });

        });
    </script>


@endsection


@section('styles')

    <link href="{{asset('/static/dist/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>


@endsection