@extends('dashboard.base')


@section('contents')



    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Create New Announcement</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Dashboard</a></li>
                <li><a href="#"><span>Announcement</span></a></li>
                <li class="active"><span>Create New Announcement</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>




    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">


                        <div class="row">



                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif


                                        <form
                                              role="form" method="post"
                                              action="{{ route('super.announcements.announcements.save') }}"
                                        >


                                        {{ csrf_field() }}

                                        <div class="form-body">

                                            <hr class="light-grey-hr"/>



                                            <div class="row">



                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Title</label>
                                                        <input type="text" name="name" required class="form-control title" placeholder="Title">

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Slug</label>
                                                        <input type="text" name="slug"  required class="form-control slug">

                                                    </div>
                                                </div>




                                                <!--/span-->



                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Description</label>
                                                        <input type="text"  name="description" class="form-control" placeholder="Description">

                                                    </div>
                                                </div>










                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->





                                        </div>










                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







    <div class="row">

    <div class="col-sm-12">
        <div class="panel panel-default card-view">
            <div class="panel-wrapper collapse in">
                <div class="panel-body row">

                    <div class="table-wrap">
                        <div class="table-responsive">
                            <table class="table display product-overview mb-30" id="datable_1">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>title</th>
                                    <th>Category</th>
                                    <th>Description</th>
                                    <th>Delete</th>

                                </tr>
                                </thead>
                                <tfoot>

                                </tfoot>
                                <tbody>
                                @foreach($a as $emp)
                                    <tr>



                                        <td>
                                            {{$emp->id}}
                                        </td>

                                        <td>
                                            {{$emp->title}}
                                        </td>

                                        <td>
                                            {{$emp->category}}
                                        </td>


                                        <td>{{$emp->description}}</td>

                                        <td>
                                            <a target="_blank" href="{{route('super.announcements.announcements.delete',$emp->id)}}" class="text-inverse"
                                               title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete txt-danger"></i></a></td>
                                    </tr>

                                @endforeach



                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    </div>

@endsection


@section('scripts')

    <script>



        $(document).ready(function(){
            $(".title").keyup(function(){

                const cat_name_val = $( this ).val();
                const actualSlug = cat_name_val.replace(/ /g,'-').toLowerCase()

                $(".slug").val(actualSlug);



            });
        });


    </script>


@endsection