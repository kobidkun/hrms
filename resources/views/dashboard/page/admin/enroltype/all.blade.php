@extends('dashboard.base')


@section('contents')



    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">

            <a class="btn btn-primary" href="{{route('super.enrollment.master.create')}}">Create Enrollment Types</a>

        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Dashboard</a></li>
                <li><a href="#"><span>Master</span></a></li>
                <li class="active"><span>Employment Types</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>







                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-default card-view">
                                    <div class="panel-heading">
                                        <div class="pull-left">
                                            <h6 class="panel-title txt-dark">Employment Types</h6>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <div class="table-wrap">
                                                <div class="table-responsive">






                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col">Title</th>
                                                            <th scope="col">Description</th>
                                                            <th scope="col">Handle</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>


                                                        @foreach($a as $j)
                                                        <tr>
                                                            <th scope="row">1</th>
                                                            <td>{{$j->name}}</td>
                                                            <td>{{$j->description}}</td>
                                                            <td>

                                                                <a class="btn btn-danger" href="{{route('super.enrollment.master.delete',$j->id)}}">
                                                                    Delete
                                                                </a>


                                                            </td>
                                                        </tr>

                                                        @endforeach

                                                        </tbody>
                                                    </table>






                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>








@endsection






@section('styles')

    <link href="{{asset('/static/dist/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>


@endsection