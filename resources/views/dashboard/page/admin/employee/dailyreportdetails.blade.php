@extends('dashboard.base')


@section('contents')



    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Employee Details</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Dashboard</a></li>
                <li><a href="#"><span>Employee</span></a></li>
                <li class="active"><span>Daily Upload  Details</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>







    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="item-big">
                                    <!-- START carousel-->
                                    <div id="carousel-example-captions-1" data-ride="carousel" class="carousel slide">
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-captions-1" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-captions-1" data-slide-to="1"></li>
                                        </ol>
                                        <div role="listbox" class="carousel-inner">
                                            <div class="item active">

                                                @if( $r->image_location != null)
                                                    <img src="/storage/{{$r->image_location}}" alt=""/>
                                                @endif


                                                    @if( $r->image_location === null )

                                                        <img src="{{asset('images/placeholder/user.png')}}" alt="First slide image">
                                                @endif







                                            </div>

                                        </div>
                                    </div>
                                    <!-- END carousel-->
                                </div>








                            </div>

                            <div class="col-md-9">
                                <div class="product-detail-wrap">



                                    <h3 class="mb-20 weight-500"></h3>


                                    <div class="product-price head-font mb-30"><span style="color: #000000"> {{$r->title}}</span> </div>
                                    <div class="product-price head-font mb-30">
                                        <p>{{$r->description}}</p>
                                    </div>
                                    <div class="product-price head-font mb-30"><span style="color: #000000">IP: {{$r->ip}}



                                        </span>
                                    </div>


                                    <p>
                                        ISP name : {{$res->isp}} <br>
                                        ISP city : {{$res->city}} <br>
                                        ISP country : {{$res->country}} <br>
                                        ISP org : {{$res->org}} <br>


                                    </p>



                                    <div class="product-price head-font mb-30"><span style="color: #000000">Date Time: {{$r->created_at}}</span></div>









                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->

    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Uploaded Images</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div  class="panel-body">




                        <div class="col-sm-12 mt-40">

                            <div class="row ">
                                @foreach($imgs as $a)

                                    <div class="col-xs-6 col-sm-4">


                                        <img src="http://{{$a->image_location}}" width="250px" alt=""/>

                                        <br>
                                        {{$a->image_title}}

                                        <br>
                                        {{$a->created_at}}

                                        <br>
                                        <a href="{{route('super.employee.image.get.delete.daily.report',$a->id)}}" class="btn btn-danger" >Delete</a>







                                    </div>

                                @endforeach





                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Uploaded Files</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div  class="panel-body">




                        <div class="col-sm-12 mt-40">

                            <div class="row ">
                                @foreach($files as $a)

                                    <div class="col-xs-6 col-sm-3">



                                        <a href="http://{{$a->file_location}}" class="btn btn-primary btn-block">


                                            {{$a->file_title}}
                                        </a>


                                        <br>
                                        {{$a->created_at}}

                                        <br>

                                        <a class="btn btn-danger" href="{{route('super.employee.file.get.delete.daily.report',$a->id)}}">Delete</a>







                                    </div>

                                @endforeach





                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        <div style="height: 450px; width: 100%;" id="map">
                            map
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>








@endsection




@section('scripts')



    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBg05xTiZrszAOhy8RnF9LbFJSvcE0ZHvs&callback=initMap"
            type="text/javascript"></script>



    <script>



        // var map;
        // var myLatlng = new google.maps.LatLng(26.6916003,88.4213799);
        function initMap() {
            var uluru = {lat: {{$r->lang}}, lng: {{$r->long}}};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: uluru
            });

            var contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h1 id="firstHeading" class="firstHeading">Daily Report post Location</h1>'+
                '<div id="bodyContent">'+
                '<p>{{$r->created_at}}</p>'+
                '</div>'+
                '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            var marker = new google.maps.Marker({
                position: uluru,
                map: map,
                title: 'Uluru (Ayers Rock)'
            });
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
        }

    </script>








@endsection



@section('styles')




@endsection