@extends('dashboard.base')


@section('contents')



    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Employee Details</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Dashboard</a></li>
                <li><a href="#"><span>Employee</span></a></li>
                <li class="active"><span>Employee Details</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>







    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="item-big">
                                    <!-- START carousel-->
                                    <div id="carousel-example-captions-1" data-ride="carousel" class="carousel slide">
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-captions-1" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-captions-1" data-slide-to="1"></li>
                                        </ol>
                                        <div role="listbox" class="carousel-inner">
                                            <div class="item active">

                                                @if( !empty($employee->create_employee_to_pictures))
                                                    <img src="http://{{$employee->create_employee_to_pictures->image_location}}" alt=""/>
                                                @endif


                                                    @if( empty($employee->create_employee_to_pictures))
                                                        <img src="{{asset('images/placeholder/user.png')}}" alt="First slide image">
                                                @endif







                                            </div>

                                        </div>
                                    </div>
                                    <!-- END carousel-->
                                </div>



                                    <div class="panel panel-default card-view">

                                        <div  class="panel-wrapper collapse in">
                                            <div  class="panel-body">
                                                <!-- sample modal content -->
                                                <div class="modal fade bs-example-modal-lg"
                                                     tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">




                                                    <div class="modal-dialog modal-lg">





                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h5 class="modal-title" id="myLargeModalLabel">Upload Primary Image Here</h5>
                                                            </div>
                                                            <div class="modal-body">



                                                                <form method="POST" action="{{ route('super.employee.upload.profile') }}"
                                                                      class="m-dropzone dropzone m-dropzone--primary"
                                                                      id="mDropzoneTwoPrimary"
                                                                >

                                                                    {{ csrf_field() }}
                                                                    <input name="id" value="{{$employee->id}}" type="hidden">
                                                                    <div class="m-dropzone__msg dz-message needsclick">
                                                                        <h3 class="m-dropzone__msg-title">
                                                                            Drop Primary Image here or click to upload.
                                                                        </h3>
                                                                        <span class="m-dropzone__msg-desc">
														Primary Image
													</span>
                                                                    </div>
                                                                </form>








                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>








                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <!-- /.modal -->
                                                <!-- Button trigger modal -->
                                               <div class="align-center">
                                                   <button class="btn btn-primary" data-toggle="modal"
                                                           data-target=".bs-example-modal-lg" > Edit Image</button>
                                               </div>
                                            </div>
                                        </div>
                                    </div>






                            </div>

                            <div class="col-md-9">
                                <div class="product-detail-wrap">



                                    <h3 class="mb-20 weight-500">{{$employee->fname }} {{$employee->mname }} {{$employee->lname }}</h3>


                                    <div class="product-price head-font mb-30"><span style="color: #000000">Enrolment id:</span> {{$employee->enrollment_id }}</div>
                                    <div class="product-price head-font mb-30"><span style="color: #000000">Mobile:</span> {{$employee->mobile }}</div>
                                    <div class="product-price head-font mb-30"><span style="color: #000000">Email: </span>{{$employee->email }}</div>


                                    <div class="col-md-4">

                                    <a class="btn btn-success" href="{{route('super.employee.mapview',$employee->id)}}">View in Map</a>


                                    </div>


                                    <div class="col-md-4">

                                    <a class="btn btn-danger" href="{{route('super.employee.delete',$employee->id)}}">Delete Employee</a>


                                    </div>


                                    <div class="col-md-4">

                                    <div class="modal fade bs-example-modal-lg-mapviewbydate"
                                         tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">




                                        <div class="modal-dialog modal-lg">





                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h5 class="modal-title" id="myLargeModalLabel">Select Date</h5>
                                                </div>
                                                <div class="modal-body">



                                                    <form method="POST" action="{{ route('super.employee..date.mapview',$employee->id) }}"

                                                    >

                                                        {{ csrf_field() }}

                                                        <div class="form-group">
                                                            <label class="control-label mb-10">Date</label>

                                                            <div class='input-group date' id='enrollment'>
                                                                <input name="date"  value="" id="enrollment"  type='text'  class="form-control" />
                                                                <span class="input-group-addon">
																	<span class="fa fa-calendar"></span>
																</span>
                                                            </div>

                                                        </div>

                                                        <button type="submit" class="btn-success">Submit</button>

                                                    </form>








                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>








                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                        <div class="align-center">
                                        <button class="btn btn-primary" data-toggle="modal"
                                                data-target=".bs-example-modal-lg-mapviewbydate" > View By Date</button>
                                    </div>

                                    </div>











                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div  class="tab-struct custom-tab-1 product-desc-tab">
                            <ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_7">




                                <li class="active" role="presentation">
                                    <a  data-toggle="tab" id="adi_info_tab" role="tab" href="#dailyreport" aria-expanded="false"><span>Daily Report </span></a></li>




                                <li role="presentation" class="next">
                                    <a  data-toggle="tab" id="adi_info_tab"
                                        role="tab" href="#additional"
                                        aria-expanded="false"><span>Additional Details</span></a>

                                </li>

                                <li role="presentation" class="next">
                                    <a  data-toggle="tab" id="adi_info_tab" role="tab" href="#adddoccuments" aria-expanded="false"><span>Doccuments</span></a>

                                </li>







                                <li role="presentation" class="">
                                    <a  data-toggle="tab" id="review_tab" role="tab" href="#editprofile" aria-expanded="false"><span>Edit Profile</span></a>
                                </li>

                                <li role="presentation" class="">
                                    <a  data-toggle="tab" id="review_tab" role="tab" href="#passwordchange" aria-expanded="false">
                                        <span>Change Password</span></a>
                                </li>


                            </ul>






                            <div class="tab-content" id="myTabContent_7">














                                <div  id="dailyreport" class="tab-pane fade active in pt-0" role="tabpanel">



                                    <div class="col-sm-12">
                                        <div class="panel panel-default card-view">
                                            <div class="panel-heading">
                                                <div class="pull-left">
                                                    <h6 class="panel-title txt-dark">Employee Daily Report</h6>
                                                </div>

                                            </div>

                                            <div class="table-wrap">
                                                <div class="table-responsive">
                                                    <table id="dailyreporttavle" class="table table-hover table-bordered display mb-30" >


                                                        <thead>

                                                        <tr>
                                                            <th>Sno</th>
                                                            <th>Title</th>
                                                            <th>Action</th>
                                                            <th>Date</th>



                                                        </tr>
                                                        </thead>




                                                        <tfoot>
                                                        <tr>
                                                            <th>Sno</th>
                                                            <th>Title</th>
                                                            <th>Action</th>
                                                            <th>Date</th>

                                                        </tr>
                                                        </tfoot>



                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-12">
                                        <div class="panel panel-default card-view">
                                            <div class="panel-heading">
                                                <div class="pull-left">
                                                    <h6 class="panel-title txt-dark">Employee Leads</h6>
                                                </div>

                                            </div>

                                            <div class="table-wrap">
                                                <div class="table-responsive">
                                                    <table id="employeeleads" class="table table-hover table-bordered display mb-30" >


                                                        <thead>

                                                        <tr>
                                                            <th>Sno</th>
                                                            <th>Name</th>
                                                            <th>Mobile</th>
                                                            <th>Email</th>

                                                            <th>Action</th>
                                                            <th>Date</th>



                                                        </tr>
                                                        </thead>




                                                        <tfoot>
                                                        <tr>
                                                            <th>Sno</th>
                                                            <th>Name</th>
                                                            <th>Mobile</th>
                                                            <th>Email</th>

                                                            <th>Action</th>
                                                            <th>Date</th>

                                                        </tr>
                                                        </tfoot>



                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>



                                    <div class="col-sm-12">
                                        <div class="panel panel-default card-view">
                                            <div class="panel-heading">
                                                <div class="pull-left">
                                                    <h6 class="panel-title txt-dark">Employee Expences</h6>
                                                </div>

                                            </div>

                                            <div class="table-wrap">
                                                <div class="table-responsive">
                                                    <table id="employeeexpences" class="table table-hover table-bordered display mb-30" >


                                                        <thead>

                                                        <tr>
                                                            <th>Sno</th>
                                                            <th>Title</th>

                                                            <th>Price</th>

                                                            <th>Action</th>
                                                            <th>Date</th>



                                                        </tr>
                                                        </thead>




                                                        <tfoot>
                                                        <tr>
                                                            <th>Sno</th>
                                                            <th>Title</th>
                                                            <th>Price</th>

                                                            <th>Action</th>
                                                            <th>Date</th>

                                                        </tr>
                                                        </tfoot>



                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>













                                    <div class="col-sm-12">
                                        <div class="panel panel-default card-view">
                                            <div class="panel-heading">
                                                <div class="pull-left">
                                                    <h6 class="panel-title txt-dark">Employee Request</h6>
                                                </div>

                                            </div>

                                            <div class="table-wrap">
                                                <div class="table-responsive">
                                                    <table id="employeerequest" class="table table-hover table-bordered display mb-30" >


                                                        <thead>

                                                        <tr>
                                                            <th>Sno</th>
                                                            <th>Name</th>
                                                            <th>Mobile</th>
                                                            <th>Email</th>

                                                            <th>Action</th>
                                                            <th>Date</th>



                                                        </tr>
                                                        </thead>




                                                        <tfoot>
                                                        <tr>
                                                            <th>Sno</th>
                                                            <th>Name</th>
                                                            <th>Mobile</th>
                                                            <th>Email</th>

                                                            <th>Action</th>
                                                            <th>Date</th>

                                                        </tr>
                                                        </tfoot>



                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>






                                </div>






                                <div  id="additional" class="tab-pane pt-0 fade" role="tabpanel">




                                    @if( !empty($employee->create_employee_to_additional_profiles))



                                        <div class="col-sm-12 mt-40">
                                            <div class="row grid-block">
                                                <div class="col-xs-6 col-sm-3"> Basic Salary: {{$employee->create_employee_to_additional_profiles->basic_salary}} </div>
                                                <div class="col-xs-6 col-sm-3"> Other Allowance: {{$employee->create_employee_to_additional_profiles->other_allowance}} </div>
                                                <div class="col-xs-6 col-sm-3"> Bank Account No: {{$employee->create_employee_to_additional_profiles->bank_account_no}} </div>
                                                <div class="col-xs-6 col-sm-3"> Bank Account IFSC: {{$employee->create_employee_to_additional_profiles->bank_account_ifsc}} </div>
                                                <div class="col-xs-6 col-sm-3"> Bank Account Branch: {{$employee->create_employee_to_additional_profiles->bank_account_branch}} </div>
                                                <div class="col-xs-6 col-sm-3"> Bank Name : {{$employee->create_employee_to_additional_profiles->bank_account_bank_name}} </div>
                                                <div class="col-xs-6 col-sm-3"> Qualification: {{$employee->create_employee_to_additional_profiles->qualification}} </div>
                                                <div class="col-xs-6 col-sm-3"> Details: {{$employee->create_employee_to_additional_profiles->details}} </div>
                                                <div class="col-xs-6 col-sm-3"> School: {{$employee->create_employee_to_additional_profiles->school}} </div>
                                                <div class="col-xs-6 col-sm-3"> College: {{$employee->create_employee_to_additional_profiles->college}} </div>
                                                <div class="col-xs-6 col-sm-3"> Experience 1: {{$employee->create_employee_to_additional_profiles->experience1}} </div>
                                                <div class="col-xs-6 col-sm-3"> Experience 2: {{$employee->create_employee_to_additional_profiles->experience2}} </div>
                                                <div class="col-xs-6 col-sm-3"> Experience 3: {{$employee->create_employee_to_additional_profiles->experience3}} </div>
                                                <div class="col-xs-6 col-sm-3"> Experience 4: {{$employee->create_employee_to_additional_profiles->experience4}} </div>
                                                <div class="col-xs-6 col-sm-3"> Experience 5: {{$employee->create_employee_to_additional_profiles->experience5}} </div>
                                                <div class="col-xs-6 col-sm-3"> Reference 1: {{$employee->create_employee_to_additional_profiles->reference}} </div>
                                                <div class="col-xs-6 col-sm-3"> Reference 2: {{$employee->create_employee_to_additional_profiles->reference2}} </div>
                                                <div class="col-xs-6 col-sm-3"> Reference 3: {{$employee->create_employee_to_additional_profiles->reference3}} </div>
                                                <div class="col-xs-6 col-sm-3"> Reference 4: {{$employee->create_employee_to_additional_profiles->reference4}} </div>


                                            </div>
                                        </div>





                                        <div class="modal fade additionalprofileedit"
                                             tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">




                                            <div class="modal-dialog modal-lg">





                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account mr-10"></i>Employee's Additional Info</h6>
                                                    </div>
                                                    <div class="modal-body">





                                                        <form
                                                                role="form" method="post"
                                                                action="{{ route('super.employee.additional.profile.edit', $employee->create_employee_to_additional_profiles->id) }}"
                                                        >


                                                            {{ csrf_field() }}

                                                            <div class="form-body">

                                                                <hr class="light-grey-hr"/>



                                                                <div class="row">



                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="control-label mb-10">Basic Salary</label>
                                                                            <input type="text" value="{{$employee->create_employee_to_additional_profiles->basic_salary}}" name="basic_salary" class="form-control" placeholder="Basic Salary">
                                                                            <input type="hidden"  name="create_employee_id" value="{{$employee->id}}" class="form-control" placeholder="Amit">

                                                                        </div>
                                                                    </div>




                                                                    <!--/span-->



                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="control-label mb-10">Other Allowance</label>
                                                                            <input type="text"value="{{$employee->create_employee_to_additional_profiles->other_allowance}}" name="other_allowance" class="form-control" placeholder="Other Allowance">

                                                                        </div>
                                                                    </div>



                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">Bank Account No</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->bank_account_no}}"
                                                                                   name="bank_account_no"  class="form-control" placeholder="Bank Account No">

                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">Bank Account IFSC</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->bank_account_ifsc}}"
                                                                                   name="bank_account_ifsc"  class="form-control" placeholder="Bank Account IFSC">

                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">Bank Account Branch</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->bank_account_branch}}"
                                                                                   name="bank_account_branch"  class="form-control" placeholder="Bank Account Branch">

                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">Bank Name</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->bank_account_bank_name}}"
                                                                                   name="bank_account_bank_name"   class="form-control" placeholder="Bank Name">

                                                                        </div>
                                                                    </div>




                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">Qualification</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->qualification}}"
                                                                                   name="qualification"   class="form-control" placeholder="Qualification">

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">Other Details</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->basic_salary}}"
                                                                                   name="details"   class="form-control" placeholder="Other Details">

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">School</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->school}}"
                                                                                   name="school"   class="form-control" placeholder="School">

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">College</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->college}}"
                                                                                   name="college"   class="form-control" placeholder="College">

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">Reference 1</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->experience1}}"
                                                                                   name="experience1"   class="form-control" placeholder="Experience">

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">Experience 2</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->experience2}}"
                                                                                   name="experience2"   class="form-control" placeholder="Experience">

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">Reference 1</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->reference}}"
                                                                                   name="reference"   class="form-control" placeholder="Reference">

                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">Reference 2</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->reference3}}"
                                                                                   name="reference2"   class="form-control" placeholder="Reference">

                                                                        </div>
                                                                    </div>





                                                                    <div class="col-md-4">
                                                                        <div class="form-group ">
                                                                            <label class="control-label mb-10">Reference 3</label>
                                                                            <input type="text"
                                                                                   value="{{$employee->create_employee_to_additional_profiles->reference3}}"
                                                                                   name="reference3"   class="form-control" placeholder="Reference">

                                                                        </div>
                                                                    </div>






                                                                    <!--/span-->
                                                                </div>
                                                                <!-- /Row -->


















                                                            </div>










                                                            <div class="form-actions mt-10">
                                                                <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                                                <button type="reset" class="btn btn-default">Cancel</button>
                                                            </div>
                                                        </form>



                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>








                                                <!-- /.modal-content -->
                                            </div>







                                            <!-- /.modal-dialog -->
                                        </div>




                                        <div class="col-sm-12 mt-40">


                                            <div class="col-md-4">


                                            </div>



                                            <div class="col-md-4">

                                                <button class="btn btn-primary" data-toggle="modal"
                                                        data-target=".additionalprofileedit" > Edit Details</button>

                                            </div>


                                            <div class="col-md-4">


                                            </div>








                                        </div>


                                    @endif


                                    @if( empty($employee->create_employee_to_additional_profiles))

                                                                                       <div class="col-sm-12 mt-40">


                                                <div class="col-md-4">


                                                </div>



                                                <div class="col-md-4">

                                                    <button class="btn btn-primary" data-toggle="modal"
                                                            data-target=".additionalprofileadd" > Add Details</button>

                                                </div>


                                                <div class="col-md-4">


                                                </div>








                                            </div>




                                            <div class="modal fade additionalprofileadd"
                                                 tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">




                                                <div class="modal-dialog modal-lg">





                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account mr-10"></i>Employee's Additional Info</h6>
                                                        </div>
                                                        <div class="modal-body">





                                                            <form
                                                                    role="form" method="post"
                                                                    action="{{ route('super.employee.additional.profile.save') }}"
                                                            >


                                                                {{ csrf_field() }}

                                                                <div class="form-body">

                                                                    <hr class="light-grey-hr"/>



                                                                    <div class="row">



                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label class="control-label mb-10">Basic Salary</label>
                                                                                <input type="text" name="basic_salary" class="form-control" placeholder="Amit">
                                                                                <input type="hidden" name="create_employee_id" value="{{$employee->id}}" class="form-control" placeholder="Amit">

                                                                            </div>
                                                                        </div>




                                                                        <!--/span-->



                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label class="control-label mb-10">Other Allowance</label>
                                                                                <input type="text"  name="other_allowance" class="form-control" placeholder="Other Allowance">

                                                                            </div>
                                                                        </div>



                                                                        <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">Bank Account No</label>
                                                                                <input type="text" name="bank_account_no"  class="form-control" placeholder="Bank Account No">

                                                                            </div>
                                                                        </div>


                                                                        <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">Bank Account IFSC</label>
                                                                                <input type="text" name="bank_account_ifsc"  class="form-control" placeholder="Bank Account IFSC">

                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">Bank Account Branch</label>
                                                                                <input type="text" name="bank_account_branch"  class="form-control" placeholder="Bank Account Branch">

                                                                            </div>
                                                                        </div>


                                                                        <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">Bank Name</label>
                                                                                <input type="text" name="bank_account_bank_name"   class="form-control" placeholder="Bank Name">

                                                                            </div>
                                                                        </div>




                                                                        <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">Qualification</label>
                                                                                <input type="text" name="qualification"   class="form-control" placeholder="Qualification">

                                                                            </div>
                                                                        </div>
                                                                         <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">Other Details</label>
                                                                                <input type="text" name="details"   class="form-control" placeholder="Other Details">

                                                                            </div>
                                                                        </div>
                                                                         <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">School</label>
                                                                                <input type="text" name="school"   class="form-control" placeholder="School">

                                                                            </div>
                                                                        </div>
                                                                         <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">College</label>
                                                                                <input type="text" name="college"   class="form-control" placeholder="College">

                                                                            </div>
                                                                        </div>
                                                                         <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">Reference 1</label>
                                                                                <input type="text" name="experience1"   class="form-control" placeholder="Experience">

                                                                            </div>
                                                                        </div>
                                                                         <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">Experience 2</label>
                                                                                <input type="text" name="experience2"   class="form-control" placeholder="Experience">

                                                                            </div>
                                                                        </div>
                                                                         <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">Reference 1</label>
                                                                                <input type="text" name="reference"   class="form-control" placeholder="Reference">

                                                                            </div>
                                                                        </div>

                                                                         <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">Reference 2</label>
                                                                                <input type="text" name="reference2"   class="form-control" placeholder="Reference">

                                                                            </div>
                                                                        </div>





                                                                        <div class="col-md-4">
                                                                            <div class="form-group ">
                                                                                <label class="control-label mb-10">Reference 3</label>
                                                                                <input type="text" name="reference3"   class="form-control" placeholder="Reference">

                                                                            </div>
                                                                        </div>






                                                                        <!--/span-->
                                                                    </div>
                                                                    <!-- /Row -->


















                                                                </div>










                                                                <div class="form-actions mt-10">
                                                                    <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                                                    <button type="reset" class="btn btn-default">Cancel</button>
                                                                </div>
                                                            </form>



                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>








                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>











                                    @endif















                                </div>


                                <div  id="adddoccuments" class="tab-pane pt-0 fade" role="tabpanel">


                                    <form
                                            role="form" method="post"
                                            enctype="multipart/form-data"
                                            action="{{ route('super.employee.doccument.employee.save', $employee->id) }}"
                                    >


                                        {{ csrf_field() }}

                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-file-add mr-10"></i>Employee's Doccuments</h6>
                                            <hr class="light-grey-hr"/>



                                            <div class="row">



                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Doccuments Details</label>
                                                        <input type="text" required name="name"  class="form-control" placeholder="Doccuments Details">

                                                    </div>
                                                </div>



                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10"> Doccuments Description</label>
                                                        <input type="text" name="description"  class="form-control" placeholder="Doccuments Description">

                                                    </div>
                                                </div>



                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10"> Choose Doccuments</label>
                                                        <input type="file" required name="image"  class="form-control" placeholder="Choose Doccuments">

                                                    </div>
                                                </div>







                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <div class="form-actions mt-10">
                                                            <button type="submit" class="btn btn-success  mr-10"> Sumbmit</button>

                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </form>





                                        <div class="col-sm-12">
                                            <div class="panel panel-default card-view">
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body row">

                                                            <div class="table-wrap">
                                                                <div class="table-responsive">
                                                                    <table class="table display product-overview mb-30" id="datable_1">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Photo</th>
                                                                            <th>Name</th>
                                                                            <th>Details</th>
                                                                            <th>Download</th>

                                                                        </tr>
                                                                        </thead>
                                                                        <tfoot>

                                                                        </tfoot>
                                                                        <tbody>
                                                                        @foreach($employee->create_employee_to_doccuments as $emp)
                                                                        <tr>



                                                                            <td>
                                                                                <img src="http://{{$emp->location}}" alt="iMac" width="80">
                                                                            </td>
                                                                            <td class="txt-dark">{{$emp->name}}</td>

                                                                            <td>{{$emp->description}}</td>

                                                                            <td>
                                                                                <a target="_blank" href="http://{{$emp->location}}" class="text-inverse"
                                                                                   title="Download" data-toggle="tooltip"><i class="zmdi zmdi-download txt-success"></i></a></td>
                                                                        </tr>

                                                                            @endforeach



                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>




                                </div>


                                <div  id="passwordchange" class="tab-pane pt-0 fade" role="tabpanel">


                                    <form
                                            role="form" method="post"
                                            enctype="multipart/form-data"
                                            action="{{ route('super.employee.password.change', $employee->id) }}"
                                    >


                                        {{ csrf_field() }}

                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-file-add mr-10"></i>Change Password</h6>
                                            <hr class="light-grey-hr"/>



                                            <div class="row">



                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">New Password</label>
                                                        <input type="password" required name="password"
                                                               class="form-control" placeholder="New Password">

                                                    </div>
                                                </div>











                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <div class="form-actions mt-10">
                                                            <button type="submit" class="btn btn-success  mr-10"> Sumbmit</button>

                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </form>










                                </div>




                                <div  id="editprofile" class="tab-pane pt-0 fade" role="tabpanel">


                                    <form
                                            role="form" method="post"
                                            action="{{ route('super.employee.edit',$employee->id) }}"
                                    >


                                        {{ csrf_field() }}

                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account mr-10"></i>Employee's Info</h6>
                                            <hr class="light-grey-hr"/>



                                            <div class="row">



                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">First Name</label>
                                                        <input type="text" name="fname" value="{{$employee->fname}}" class="form-control" placeholder="Amit">

                                                    </div>
                                                </div>




                                                <!--/span-->



                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Middle Name</label>
                                                        <input type="text" value="{{$employee->mname}}"  name="mname" class="form-control" placeholder="Kumar">

                                                    </div>
                                                </div>



                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label mb-10">Last Name</label>
                                                        <input type="text" value="{{$employee->lname}}"  name="lname"  class="form-control" placeholder="Jain">

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label mb-10">Mobile</label>
                                                        <input type="text" value="{{$employee->mobile}}"   name="mobile"  class="form-control" placeholder="Mobile">

                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label mb-10">Emergency Contact</label>
                                                        <input type="text" value="{{$employee->phone}}"   name="phone"  class="form-control" placeholder="Emergency Contact">

                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <label class="control-label mb-10">Email</label>
                                                        <input type="email" value="{{$employee->email}}"   name="email"   class="form-control" placeholder="Email">

                                                    </div>
                                                </div>






                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->









                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Gender</label>
                                                        <select name="gender" class="form-control">
                                                            <option selected value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                        </select>
                                                        <span class="help-block"> Select Employee gender </span>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Date of Birth</label>

                                                        <div class='input-group date' id='birthdaydate'>
                                                            <input name="dob"  value="{{$employee->dob}}"   type='text'  class="form-control" />
                                                            <span class="input-group-addon">
																	<span class="fa fa-calendar"></span>
																</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>




                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Enrollment Date</label>
                                                        <div class='input-group date' id='enrollment'>
                                                            <input name="enrollment_date" type='text'  value="{{$employee->enrollment_date}}"   class="form-control" />
                                                            <span class="input-group-addon">
																	<span class="fa fa-calendar"></span>
																</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Category</label>
                                                        <select name="category" class="form-control">
                                                            <option selected value="Sales">{{$employee->category}}</option>
                                                            @foreach($c as $cs)



                                                                <option value="{{$cs->name}}">{{$cs->name}}</option>



                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->

                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <label class="control-label mb-10">Enrollment ID</label>
                                                        <input type="text" name="enrollment_id"  value="{{$employee->enrollment_id}}"    class="form-control"
                                                               placeholder="Enrollment ID">

                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>





                                            <!-- /Row -->

                                            <!-- /Row -->

                                            <div class="seprator-block"></div>

                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>Local Address</h6>
                                            <hr class="light-grey-hr"/>
                                            <div class="row">


                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 street">Street</label>
                                                        <input name="present_address_street"  value="{{$employee->present_address_street}}"  type="text"
                                                               class="form-control present_address_street">
                                                    </div>
                                                </div>


                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Locality</label>
                                                        <input name="present_address_locality"  value="{{$employee->present_address_locality}}"   type="text"

                                                               class="form-control present_address_locality">
                                                    </div>
                                                </div>


                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Area</label>
                                                        <input name="present_address_area"  value="{{$employee->present_address_area}}"   type="text"
                                                               class="form-control present_address_area">
                                                    </div>
                                                </div>



                                            </div>



                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">City</label>
                                                        <input name="present_address_city"  value="{{$employee->present_address_city}}"   type="text"
                                                               class="form-control present_address_city">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">State</label>
                                                        <input name="present_address_state"  value="{{$employee->present_address_state}}"   type="text"
                                                               class="form-control present_address_state">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>



                                            <!-- /Row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Post Code</label>
                                                        <input name="present_address_pin"  value="{{$employee->present_address_pin}}"   type="text"
                                                               class="form-control present_address_pin">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Country</label>
                                                        <select name="present_address_country" class="form-control present_address_country">
                                                            <option>--Select your Country--</option>
                                                            <option selected>India</option>
                                                            <option>Nepal</option>
                                                            <option>Bhutan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>












                                            <div class="seprator-block"></div>

                                            <div class="col-md-8 ">
                                                <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>

                                                    Permanent  Address

                                                </h6>
                                            </div>

                                            <div class="col-md-4 ">

                                                <button class="btn btn-success btn-outline btn-icon right-icon copyaddress">
                                                    <span>Copy From Present Address</span><i class="fa fa-paste"></i></button>


                                            </div>


                                            <hr class="light-grey-hr"/>
                                            <div class="row">


                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Street</label>
                                                        <input name="permanent_address_street"  value="{{$employee->permanent_address_street}}"   type="text" class="form-control permanent_address_street">
                                                    </div>
                                                </div>


                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Locality</label>
                                                        <input name="permanent_address_locality"  value="{{$employee->permanent_address_locality}}"   type="text" class="form-control permanent_address_locality">
                                                    </div>
                                                </div>


                                                <div class="col-md-4 ">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Area</label>
                                                        <input name="permanent_address_area" value="{{$employee->permanent_address_area}}"   type="text" class="form-control permanent_address_area">
                                                    </div>
                                                </div>



                                            </div>



                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">City</label>
                                                        <input name="permanent_address_city"  value="{{$employee->permanent_address_city}}"  type="text" class="form-control permanent_address_city">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">State</label>
                                                        <input name="permanent_address_state"  value="{{$employee->permanent_address_state}}"   type="text" class="form-control permanent_address_state">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>



                                            <!-- /Row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Post Code</label>
                                                        <input name="permanent_address_pin"  value="{{$employee->permanent_address_pin}}"  type="text" class="form-control permanent_address_pin">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Country</label>
                                                        <select name="permanent_address_country" class="form-control permanent_address_country">
                                                            <option>--Select your Country--</option>
                                                            <option selected>India</option>
                                                            <option>Nepal</option>
                                                            <option>Bhutan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>





                                        </div>










                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </div>
                                    </form>








                                </div>









                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>








@endsection




@section('scripts')

    <link href="{{asset('/static/dist/js/framework/bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css">

    <script src="{{asset('/static/dist/js/framework/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js')}}"></script>


    <script>
        $('#enrollment').datepicker({
            format: 'yyyy-mm-dd',
            autoclose:true,
            todayHighlight:true
        });
    </script>
    <script src="{{asset('static/dist/js/dropzone.js')}}"></script>



    <script src="{{asset('/static/dist/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $(function() {
            $('#dailyreporttavle').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('super.employee.datatable.get.daily.report',$employee->id)}}',
                order: [ [0, 'desc'] ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    { data: 'created_at', name: 'created_at' },

                ]
            });


        });


    </script>


    <script>
        $(function() {
            $('#employeerequest').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('super.employee.datatable.get.manage.request',$employee->id)}}',
                order: [ [0, 'desc'] ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    { data: 'to_request', name: 'to_request' },
                    { data: 'category', name: 'category' },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    { data: 'created_at', name: 'created_at' },

                ]
            });


        });


    </script>

    <script>
        $(function() {
            $('#employeeleads').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('super.employee.datatable.get.manage.leads',$employee->id)}}',
                order: [ [0, 'desc'] ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'mobile', name: 'mobile' },
                    { data: 'email', name: 'email' },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    { data: 'created_at', name: 'created_at' },

                ]
            });


        });
    </script>



    <script>
        $(function() {
            $('#employeeexpences').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('super.employee.datatable.get.manage.expences',$employee->id)}}',
                order: [ [0, 'desc'] ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    { data: 'price', name: 'price' },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    { data: 'created_at', name: 'created_at' },

                ]
            });


        });
    </script>







    <script>
        //== Class definition

        var DropzoneDemo = function () {
            //== Private functions
            var demos = function () {
                // single file upload
                // multiple file upload
                Dropzone.options.mDropzoneTwoPrimary = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 1, // MB
                    maxFiles: 10,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif"

                };

                Dropzone.options.mDropzoneTwo = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 5, // MB
                    maxFiles: 20,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif"

                };
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        DropzoneDemo.init();
    </script>




@endsection



@section('styles')

    <link href="{{asset('/static/dist/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('static/dist/css/dropzone.css')}}" rel="stylesheet" type="text/css"/>


@endsection