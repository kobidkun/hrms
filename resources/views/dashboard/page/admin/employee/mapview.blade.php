@extends('dashboard.base')


@section('contents')

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */

    </style>

    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Map View</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Dashboard</a></li>
                <li><a href="#"><span>Employee</span></a></li>
                <li class="active"><span>Map View</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>







                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-default card-view">

                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">



                                            <div style="height: 450px; width: 100%;" id="map">
                                                map
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>








@endsection




@section('scripts')



    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBg05xTiZrszAOhy8RnF9LbFJSvcE0ZHvs&callback=initMap"
            type="text/javascript"></script>



    <script>



       // var map;
       // var myLatlng = new google.maps.LatLng({{$loc->lat}},{{$loc->long}});
       function initMap() {
           var uluru = {lat: {{$loc->lat}}, lng: {{$loc->long}}};
           var map = new google.maps.Map(document.getElementById('map'), {
               zoom: 14,
               center: uluru
           });

           var contentString = '<div id="content">'+
               '<div id="siteNotice">'+
               '</div>'+
               '<h1 id="firstHeading" class="firstHeading">{{$e->fname}} {{$e->mname}} {{$e->lname}}</h1>'+
               '<div id="bodyContent">'+
               '<p>Last seen at {{$loc->created_at}}</p>'+
               '</div>'+
               '</div>';

           var infowindow = new google.maps.InfoWindow({
               content: contentString
           });

           var marker = new google.maps.Marker({
               position: uluru,
               map: map,
               title: 'Uluru (Ayers Rock)'
           });
           marker.addListener('click', function() {
               infowindow.open(map, marker);
           });
       }

    </script>



@endsection


@section('styles')


@endsection