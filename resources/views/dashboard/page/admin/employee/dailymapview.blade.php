@extends('dashboard.base')


@section('contents')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBg05xTiZrszAOhy8RnF9LbFJSvcE0ZHvs&callback=initMap"
            type="text/javascript"></script>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 500px;
            width: 100%;
            margin: 0px;
            padding: 0px
        }
        /* Optional: Makes the sample page fill the window. */

    </style>

    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Map View</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Dashboard</a></li>
                <li><a href="#"><span>Employee</span></a></li>
                <li class="active"><span>Map View</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>







                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-default card-view">

                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">



                                            <div id="map" style="border: 2px solid #3872ac;"></div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>





                       <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-default card-view">

                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">



                                            <div class="table-responsive">
                                                <table id="datable_2" class="table table-hover table-bordered display mb-30" >


                                                    <thead>

                                                    <tr>
                                                        <th>Sno</th>
                                                        <th>Latitude</th>
                                                        <th>Longitude</th>
                                                        <th>Date</th>
                                                        <th>Action</th>


                                                    </tr>
                                                    </thead>




                                                    <tfoot>
                                                    <tr>
                                                        <th>Sno</th>
                                                        <th>Latitude</th>
                                                        <th>Longitude</th>
                                                        <th>Date</th>
                                                        <th>Action</th>

                                                    </tr>
                                                    </tfoot>



                                                </table>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>








@endsection




@section('scripts')



    <script src="{{asset('/static/dist/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $(function() {
            $('#datable_2').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/super/employee/date/map/geo-codes-api/{{$id}}/{{$date}}',
                order: [ [0, 'desc'] ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'lat', name: 'lat' },
                    { data: 'long', name: 'long' },
                    { data: 'created_at', name: 'created_at' },
                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]
            });

        });
    </script>



    {{--
        --}}





{{--
    <script>


        var locations = [
            @forEach($ds as $d)

            ['Bondi Beach', {{$d->lat}}, {{$d->long}}, 4],

            @endforeach
        ]



        function initMap() {
            var myLatLng = {lat: 26.6916003, lng: 88.4213799};

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: myLatLng
            });

            var count;

            for (count = 0; count < locations.length; count++) {
                new google.maps.Marker({
                    position: new google.maps.LatLng(locations[count][1], locations[count][2]),
                    map: map,
                    title: locations[count][0]
                });
            }
        }

    </script>--}}



   <script>


        var MapPoints = [
                @forEach($ds as $d)

            {
                'lat' : '{{$d->lat}}',
                'long' : '{{$d->long}}',
                 'title' : '{{$d->created_at}}'
            },

            @endforeach
        ]




        var MY_MAPTYPE_ID = 'custom_style';

        function initialize() {

            if (jQuery('#map').length > 0) {

                var locations = (MapPoints);

                window.map = new google.maps.Map(document.getElementById('map'), {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false
                });

                var infowindow = new google.maps.InfoWindow();
                var flightPlanCoordinates = [];
                var bounds = new google.maps.LatLngBounds();

                var image = {
                    url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(5, 10),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(0, 32)
                };
                for (i = 0; i < locations.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i].lat, locations[i].long),
                        map: map,
                        icon: image
                    });
                    flightPlanCoordinates.push(marker.getPosition());
                    bounds.extend(marker.position);

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            infowindow.setContent(locations[i]['title']);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }

                map.fitBounds(bounds);

                var flightPath = new google.maps.Polyline({
                    map: map,
                    path: flightPlanCoordinates,
                    strokeColor: "#FF0000",
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });

            }
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>











@endsection


@section('styles')


@endsection