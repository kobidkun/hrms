let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'public/static/dist/css/style.css',
], 'public/css/style.min.css');

mix.scripts([
    'public/static/dist/js/framework/jquery.min.js',
    'public/static/dist/js/framework/bootstrap.min.js',
    'public/static/dist/js/jquery.slimscroll.js',
    'public/static/dist/js/framework/owl.carousel.min.js',
    'public/static/dist/js/framework/switchery.min.js',
    'public/static/dist/js/framework/dropdown-bootstrap-extended.js',
    'public/static/dist/js/init.js',
], 'public/js/js.min.js');
