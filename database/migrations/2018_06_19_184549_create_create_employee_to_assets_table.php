<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateEmployeeToAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_employee_to_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('create_employee_id');
            $table->text('title');
            $table->text('description');
            $table->text('reason')->nullable();
            $table->text('status')->nullable();
            $table->text('latest_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_employee_to_assets');
    }
}
