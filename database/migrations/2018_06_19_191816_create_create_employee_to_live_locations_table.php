<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateEmployeeToLiveLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_employee_to_live_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('create_employee_id');
            $table->text('lat');
            $table->text('long');
            $table->text('is_moving');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_employee_to_live_locations');
    }
}
