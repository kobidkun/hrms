<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('secure',20)->unique();
            $table->text('fname');
            $table->text('mname')->nullable();
            $table->text('lname');
            $table->text('dob');
            $table->text('enrollment_date')->nullable();
            $table->text('enrollment_id')->nullable();
            $table->text('category')->nullable();
            $table->string('email')->unique();
            $table->text('gender');
            $table->string('mobile')->unique();
            $table->text('phone')->nullable();
            $table->text('present_address_street')->nullable();
            $table->text('present_address_locality')->nullable();
            $table->text('present_address_area')->nullable();
            $table->text('present_address_city');
            $table->text('present_address_district')->nullable();
            $table->text('present_address_state')->nullable();
            $table->text('present_address_country')->nullable();
            $table->integer('present_address_pin');
            $table->text('permanent_address_street')->nullable();
            $table->text('permanent_address_locality')->nullable();
            $table->text('permanent_address_area')->nullable();
            $table->text('permanent_address_city');
            $table->text('permanent_address_district')->nullable();
            $table->text('permanent_address_state')->nullable();
            $table->text('permanent_address_country')->nullable();
            $table->integer('permanent_address_pin');
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_employees');
    }
}
