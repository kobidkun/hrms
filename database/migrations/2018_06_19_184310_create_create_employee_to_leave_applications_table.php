<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateEmployeeToLeaveApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_employee_to_leave_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('create_employee_id');
            $table->text('title');
            $table->text('description');
            $table->text('from');
            $table->text('to');
            $table->text('leave_type');
            $table->text('interval');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_employee_to_leave_applications');
    }
}
