<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateEmployeeToRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_employee_to_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('create_employee_id');
            $table->text('title');
            $table->text('category')->nullable();
            $table->text('description')->nullable();
            $table->text('image_title')->nullable();
            $table->text('image_location')->nullable();
            $table->string('to_request')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_employee_to_requests');
    }
}
