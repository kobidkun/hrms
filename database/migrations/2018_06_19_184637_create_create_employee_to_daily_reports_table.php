<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateEmployeeToDailyReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_employee_to_daily_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('create_employee_id');
            $table->string('day');
            $table->string('month');
            $table->string('year');
            $table->string('title');
            $table->text('description');
            $table->string('lang');
            $table->string('image_location');
            $table->string('long');
            $table->string('ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_employee_to_daily_reports');
    }
}
