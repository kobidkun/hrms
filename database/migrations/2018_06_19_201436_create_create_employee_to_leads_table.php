<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateEmployeeToLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_employee_to_leads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('create_employee_id');
            $table->text('name');
            $table->text('referer')->nullable();
            $table->text('mobile');
            $table->text('email')->nullable();
            $table->text('address');
            $table->text('description')->nullable();
            $table->text('image')->nullable();
            $table->text('lat')->nullable();
            $table->text('long')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_employee_to_leads');
    }
}
