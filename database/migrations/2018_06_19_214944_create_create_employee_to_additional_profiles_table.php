<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateEmployeeToAdditionalProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_employee_to_additional_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('create_employee_id');
            $table->text('basic_salary')->nullable();
            $table->text('other_allowance')->nullable();
            $table->text('bank_account_no')->nullable();
            $table->text('bank_account_ifsc')->nullable();
            $table->text('bank_account_branch')->nullable();
            $table->text('bank_account_bank_name')->nullable();
            $table->text('qualification')->nullable();
            $table->text('details')->nullable();
            $table->text('school')->nullable();
            $table->text('college')->nullable();
            $table->text('experience1')->nullable();
            $table->text('experience2')->nullable();
            $table->text('experience3')->nullable();
            $table->text('experience4')->nullable();
            $table->text('experience5')->nullable();
            $table->text('reference')->nullable();
            $table->text('reference2')->nullable();
            $table->text('reference3')->nullable();
            $table->text('reference4')->nullable();
            $table->text('reference5')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_employee_to_additional_profiles');
    }
}
