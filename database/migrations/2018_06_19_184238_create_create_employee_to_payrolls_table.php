<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateEmployeeToPayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_employee_to_payrolls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('create_employee_id');
            $table->text('date');
            $table->text('description')->nullable();
            $table->text('total');
            $table->text('allowance')->nullable();
            $table->text('deduction')->nullable();
            $table->text('adjustments')->nullable();
            $table->text('month');
            $table->text('year');
            $table->text('payment_type');
            $table->text('status');
            $table->text('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_employee_to_payrolls');
    }
}
