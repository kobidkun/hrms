<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateEmployeeToExpencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_employee_to_expences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('create_employee_id');
            $table->text('image_title')->nullable();
            $table->text('image_location')->nullable();
            $table->text('price')->nullable();
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->text('lat')->nullable();
            $table->text('long')->nullable();
            $table->text('ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_employee_to_expences');
    }
}
