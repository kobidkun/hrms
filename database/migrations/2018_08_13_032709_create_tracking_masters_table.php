<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->text('category_id')->nullable();
            $table->text('location_provider')->nullable();
            $table->text('accuracy')->nullable();
            $table->text('stationary_radius')->nullable();
            $table->text('debug')->nullable();
            $table->text('distance_filter')->nullable();
            $table->text('interval')->nullable();
            $table->text('fastest_interval')->nullable();
            $table->text('activity_interval')->nullable();
            $table->text('stop_still')->nullable();
            $table->text('start_foreground')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_masters');
    }
}
