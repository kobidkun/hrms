<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});



Route::get('/super/password-change', 'Admin\Master\MiscController@ChangeAdminPassword')->name('super.password.admin.change.view');
Route::post('/super/password-change', 'Admin\Master\MiscController@ChangeAdminPasswordStore')->name('super.password.admin.change.save');
Route::get('/super/logout', 'Admin\LogoutController@logout')->name('super.logout.get');

Route::get('/super/employee/create', 'Admin\Employee\ManageEmployee@Create')->name('super.employee.create');
Route::post('/super/employee/create', 'Admin\Employee\ManageEmployee@store')->name('super.employee.save');
Route::post('/super/employee/password/change/{id}', 'Admin\Employee\ManageEmployee@store')->name('super.employee.password.change');
Route::post('/super/employee/edit/{id}', 'Admin\Employee\ManageEmployee@editProfile')->name('super.employee.edit');
Route::get('/super/employee/delete/{id}', 'Admin\Employee\ManageEmployee@DeleteEmployee')->name('super.employee.delete');
Route::get('/super/employee/', 'Admin\Employee\ManageEmployee@All')->name('super.employee.all');
Route::get('/super/employee/api/datatable', 'Admin\Employee\ManageEmployee@Datatable')->name('super.employee.datatable');
Route::get('/super/employee/{id}', 'Admin\Employee\ManageEmployee@Details')->name('super.employee.details');


//map
Route::get('/super/employee/map/{id}', 'Admin\Employee\EmployeeMap@MapView')->name('super.employee.mapview');
Route::post('/super/employee/date/map/{id}', 'Admin\Employee\EmployeeMap@MapViewDayview')->name('super.employee..date.mapview');
Route::get('/super/employee/date/map/l-in-out/{id}/{date}', 'Admin\Employee\EmployeeMap@RequestDatatableloginlogout')
    ->name('super.employee..date.login.logout');

Route::get('/super/employee/date/map/geo-codes-api/{id}/{date}', 'Admin\Employee\EmployeeMap@RequestDatatablelivelocation')
    ->name('super.employee..date.geo.code.api');




Route::post('/super/employee/upload-profile', 'Admin\Employee\ManageEmployee@UploadProfilePicture')->name('super.employee.upload.profile');
Route::post('/super/employee/additional-profile', 'Admin\Employee\ManageEmployee@saveAdditional')
    ->name('super.employee.additional.profile.save');
Route::post('/super/employee/additional-profile/{id}', 'Admin\Employee\ManageEmployee@editsaveAdditional')
    ->name('super.employee.additional.profile.edit');
Route::post('/super/employee/employee-doccument/{id}', 'Admin\Employee\ManageEmployee@Uploaddoccument')
    ->name('super.employee.doccument.employee.save');
Route::get('/super/employee/employee-request/{id}', 'Admin\Employee\ManageEmployee@RequestDatatable')
    ->name('super.employee.doccument.get.request');


Route::get('/super/employee/employee-daily-report/{id}', 'Admin\Employee\ManageDailyReport@ViewDailyReportEmployee')
    ->name('super.employee.daily.get.report');

Route::get('/super/employee/employee-manage-leads/{id}', 'Admin\Employee\ManageLeads@ViewDailyReportEmployee')
    ->name('super.employee.request.get.leads.details');

    Route::get('/super/employee/employee-request/{id}', 'Admin\Employee\ManageRequest@ViewDailyReportEmployee')
    ->name('super.employee.request.get.details');

    Route::get('/super/employee/expences/{id}', 'Admin\Employee\ManageExpences@ViewDailyReportEmployee')
    ->name('super.employee.expences.get.details');


//employee api datatables


Route::get('/super/employee/datatable/daily-report/{id}', 'Admin\Employee\ManageDailyReport@RequestDatatable')
    ->name('super.employee.datatable.get.daily.report');

Route::get('/super/employee/datatable/manage-leads/{id}', 'Admin\Employee\ManageLeads@RequestDatatable')
    ->name('super.employee.datatable.get.manage.leads');

Route::get('/super/employee/datatable/manage-request/{id}', 'Admin\Employee\ManageRequest@RequestDatatable')
    ->name('super.employee.datatable.get.manage.request');


Route::get('/super/employee/datatable/expences/{id}', 'Admin\Employee\ManageExpences@RequestDatatable')
    ->name('super.employee.datatable.get.manage.expences');


// daily Reports

Route::get('/super/employee/file/delete/{id}', 'Admin\Employee\ManageDailyReport@DeleteFile')
    ->name('super.employee.file.get.delete.daily.report');

Route::get('/super/employee/image/delete/{id}', 'Admin\Employee\ManageDailyReport@DeleteImages')
    ->name('super.employee.image.get.delete.daily.report');



//employee api datatables

//entroolmemt


Route::get('/super/master/enrollment/', 'Admin\Master\EnrollmentTypes@allenroll')->name('super.enrollment.master.all');
Route::get('/super/master/enrollment/create', 'Admin\Master\EnrollmentTypes@Create')->name('super.enrollment.master.create');
Route::post('/super/master/enrollment/create', 'Admin\Master\EnrollmentTypes@Save')->name('super.enrollment.master.save');
Route::get('/super/master/enrollment/delete/{id}', 'Admin\Master\EnrollmentTypes@Delete')->name('super.enrollment.master.delete');

//job Titles


Route::get('/super/master/job-title/create', 'Admin\Master\ManageJobTitle@Create')->name('super.job.master.create');
Route::get('/super/master/job-title', 'Admin\Master\ManageJobTitle@AllTitles')->name('super.job.master.all');
Route::post('/super/master/job-title/create', 'Admin\Master\ManageJobTitle@Save')->name('super.job.master.save');
Route::get('/super/master/job-title/delete/{id}', 'Admin\Master\ManageJobTitle@Delete')->name('super.job.master.delete');
Route::get('/super/master/job-title/edit/{id}', 'Admin\Master\ManageJobTitle@EditCategory')->name('super.job.master.titles.edit');
Route::post('/super/master/job-title/update/{id}', 'Admin\Master\ManageJobTitle@UpdateCategory')->name('super.job.titles.master.update');

//announcements


Route::get('/super/announcements/announcement/create', 'Admin\Employee\ManageAnnouncement@Create')->name('super.announcements.announcements.create');
Route::post('/super/announcements/announcements/create', 'Admin\Employee\ManageAnnouncement@Save')->name('super.announcements.announcements.save');
Route::get('/super/announcements/announcements/delete/{id}', 'Admin\Employee\ManageAnnouncement@Delete')
    ->name('super.announcements.announcements.delete');





//// employee routes
///

Route::get('/employee/login', 'Employee\web\EmployeeAuthController@loginPage')->name('employee.login');
Route::get('/employee/dashboard', 'Employee\web\EmployeeController@dashboard')->name('employee.dashboard');
Route::post('/employee/login', 'Employee\web\EmployeeAuthController@CustomerLogin')->name('employee.login.post');
Route::get('/employee/logout', 'Employee\web\EmployeeAuthController@logout')->name('employee.logout.get');


// create daily repor

Route::get('/employee/daily-report/create', 'Employee\web\EmployeeController@CreatedaylyReportCreate')->name('employee.daily.report.create');
Route::get('/employee/daily-report/datatables', 'Employee\web\EmployeeController@DailyReportDatables')
    ->name('employee.daily.report.datatables');

Route::post('/employee/daily-report/save', 'Employee\web\EmployeeController@CreatedaylyReportsave')
    ->name('employee.daily.report.save');

Route::get('/employee/daily-report/{id}', 'Employee\web\EmployeeController@ViewDailyReport')
    ->name('employee.daily.report.details');

Route::post('/employee/daily-report/upload-image/{id}', 'Employee\web\EmployeeController@UploadImage')
    ->name('employee.daily.report.image.upload');

Route::post('/employee/daily-report/upload-file/{id}', 'Employee\web\EmployeeController@UploadFiles')
    ->name('web.employee.daily.report.files.upload');






Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');





