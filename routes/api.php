<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/employee/profile/get', 'Employee\SelfService@GetProfile');
Route::post('/employee/request/post', 'Employee\SelfService@EmployeeRequest');
Route::post('/employee/daily-report/post', 'Employee\SelfService@DailyReport');
Route::get('/employee/daily-report/get/dates/{from}/{to}', 'Employee\SelfService@DailyReportbetweentwodatesget');
Route::post('/employee/live-location/post', 'Employee\SelfService@LiveLocation');
Route::post('/employee/live-location-login-logout/post', 'Employee\SelfService@LiveLocationLogin');
Route::get('/employee/announcement/get', 'Employee\SelfService@Announcement');
Route::post('/employee/changepassword', 'Employee\SelfService@ChangePassword');
Route::get('/employee/getcategory', 'Employee\SelfService@GetCategory');



//
Route::post('/employee/expenses/post', 'Employee\SelfService@Employeeexpenses');
Route::post('/employee/leads/post', 'Employee\SelfService@EmployeeLeads');
Route::post('/employee/leads/get', 'Employee\SelfService@GetEmployeeLeads');
Route::get('/employee/master/get-category', 'Employee\MiscController@GetAllCategory');




Route::post('/employee/upload/image', 'Employee\SelfService@ImageUpload');



//

//// maarewr  ManageMaster
///

Route::get('/master', 'ManageMaster@GetAllAdmin');




Route::group(['middleware' => ['api', 'auth:api-employee']], function () {
    Route::get('/admin', function ($request) {
        // Get the logged admin instance
        return $request->user(); // You can use too `$request->user('admin')` passing the guard.
    });
});
